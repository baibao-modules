package com.baibao.option.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.baibao.option.model.Option;

@Repository
public interface OptionRepository
	extends JpaRepository<Option, Integer>, JpaSpecificationExecutor<Option>
{
	@Query(
		"SELECT o FROM Option o WHERE " +
			"o.name = ?1 ORDER BY o.updateTime DESC")
	public ArrayList<Option> getOptionsByName(String name);

	@Query(
		"SELECT o FROM Option o WHERE " +
			"o.name = ?1 AND o.effected < ?2 AND o.expired > ?2 " +
			"ORDER BY o.updateTime DESC")
	public ArrayList<Option> getValidOptionsByName(String name, Long now);
	
	@Query("SELECT o FROM Option o WHERE o.name IN (?1)")
	public ArrayList<Option> getOptionsByNames(String[] names);
}
