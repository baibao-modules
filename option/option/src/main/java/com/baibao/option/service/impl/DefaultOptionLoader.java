package com.baibao.option.service.impl;

import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import com.baibao.option.service.OptionManager;
import com.baibao.utils.FileSystem;
import com.baibao.utils.JsonHelper;

@Service("defaultOptionLoader")
public class DefaultOptionLoader
	implements InitializingBean
{
	@Autowired
	protected OptionManager optionManager;

	public void afterPropertiesSet()
		throws Exception
	{
		String jsonData =
			FileSystem.readString(
				DefaultOptionLoader.class.getResourceAsStream("/defaultOptions.json"),
				"utf-8");

		JsonObject json = JsonHelper.parseJson(jsonData).getAsJsonObject();
		for(Map.Entry<String, JsonElement> tuple: json.entrySet())
		{
			String name = tuple.getKey();
			
			if (optionManager.getValue(name) == null)
			{
				optionManager.setValue(name, tuple.getValue().getAsString());
			}
		}
	}
}
