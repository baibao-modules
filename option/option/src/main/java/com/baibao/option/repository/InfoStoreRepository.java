package com.baibao.option.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.baibao.option.model.InfoStore;

@Repository
public interface InfoStoreRepository
    extends JpaRepository<InfoStore, Long>, JpaSpecificationExecutor<InfoStore>
{
	@Query("SELECT ist FROM InfoStore ist WHERE " +
		"ist.objectType = ?1 AND ist.objectId = ?2")
	public ArrayList<InfoStore> getInfo(String objectType, String objectId);

	@Query("SELECT ist FROM InfoStore ist WHERE " +
		"ist.objectType = ?1 AND ist.objectId = ?2 AND ist.name = ?3")
	public ArrayList<InfoStore>
		getValue(String objectType, String objectId, String name);
}
