package com.baibao.option.service.impl;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.jws.WebService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.option.repository.OptionRepository;

import com.baibao.utils.Utils;
import com.baibao.utils.service.AbstractManagerImpl;

import com.baibao.option.model.Option;
import com.baibao.option.service.OptionManager;

@WebService(serviceName = "optionManager")
@Service("optionManager")
public class OptionManagerImpl
	extends AbstractManagerImpl<Option, Integer>
	implements OptionManager
{
	private static Log log = LogFactory.getLog(OptionManagerImpl.class);

	@Autowired
	protected OptionRepository optionRepository;

	@Resource(name = "redisTemplate")
	protected ValueOperations<String, String> valueOps;

	public void afterPropertiesSet()
		throws Exception
	{
		this.setRepository(optionRepository);
	}
	
	private Option newOption(String name)
	{
		Option option = new Option();

		option.setName(name);
		option.setEffected(0l);
		option.setExpired(Utils.forever().getTime());
		option.setUpdateTime(Utils.now());

		return option;
	}

	private Option getValidOption(String name)
	{
		ArrayList<Option> options =
			optionRepository.getValidOptionsByName(
				name, System.currentTimeMillis());

		return CollectionUtils.isNotEmpty(options)?
			options.get(options.size() - 1): null;
	}

	@Transactional
    public Option save(Option option)
	{
		option.setUpdateTime(Utils.now());

		return super.save(option);
    }

	@Transactional
	public Option setValue(String name, String value)
	{
		Option option = getValidOption(name);

		if (option == null)
			option = newOption(name);

		option.setValue(value);

		save(option);

		return option;
	}

	@Transactional
	public Option setValue(Integer id, String value)
	{
		Option option = get(id);
		if (option == null)
			return null;

		option.setValue(value);
		
		return save(option);
	}

	public Option getValue(String name, String defaultValue)
	{
		Option option = getValidOption(name);

		if (option == null)
		{
			if (defaultValue != null)
			{
				option = newOption(name);

				option.setValue(defaultValue);

				return option;
			}

			return null;
		}

		return option;
	}

	public Option addValue(
		String name, String value, Long effected, Long expired)
	{
		Option option = new Option();

		option.setName(name);
		option.setValue(value);
		option.setEffected(effected);
		option.setExpired(expired);
		
		return save(option);
	}

	public void remove(String name)
	{
		Option option = getValidOption(name);

		if (option == null)
			return;
		
		remove(option.getId());
	}

	public String getStringValue(String name, String defaultValue)
	{
		String value = valueOps.get("option-" + name);
		if (value != null)
			return value;

		Option option = getValue(name, defaultValue);

		if (option == null)
			return null;

		valueOps.set("option-" + name, option.getValue(), 5, TimeUnit.MINUTES);
		
		return option.getValue();
	}

	public Long getLongValue(String name, Long defaultValue) {
		String strValue = getStringValue(name, null);

		if (strValue != null)
		{
			try
			{
				return Long.parseLong(strValue);
			} catch(Exception e)
			{
				log.error(String.format(
					"error get %s long value %s", name, strValue), e);
			}
		}

		valueOps.set("option-" + name,
			String.valueOf(defaultValue), 10, TimeUnit.MINUTES);
		
		return defaultValue;
	}

	public Double getDoubleValue(String name, Double defaultValue) {
		String strValue = getStringValue(name, null);

		if (strValue != null)
		{
			try
			{
				return Double.parseDouble(strValue);
			} catch(Exception e)
			{
				log.error(String.format(
					"error get %s double value %s", name, strValue), e);
			}
		}

		valueOps.set("option-" + name,
			String.valueOf(defaultValue), 10, TimeUnit.MINUTES);

		return defaultValue;
	}

	public Boolean getBooleanValue(String name, Boolean defaultValue)
	{
		String strValue = getStringValue(name, null);

		if (strValue != null)
		{
			try
			{
				if ("true".equalsIgnoreCase(strValue) ||
					"on".equalsIgnoreCase(strValue) ||
					"yes".equalsIgnoreCase(strValue))
					return true;

				if ("false".equalsIgnoreCase(strValue) ||
					"off".equalsIgnoreCase(strValue) ||
					"no".equalsIgnoreCase(strValue))
					return false;

				return Long.parseLong(strValue) != 0l;
			} catch(Exception e)
			{
				log.error(String.format(
					"error get %s bool value %s", name, strValue), e);
			}
		}

		valueOps.set("option-" + name,
			String.valueOf(defaultValue), 10, TimeUnit.MINUTES);

		return defaultValue;
	}

	public ArrayList<Option> findOptionsByNames(String[] names)
	{
		return optionRepository.getOptionsByNames(names);
	}

	public Option getValue(String name)
	{
		return getValidOption(name);
	}
}

