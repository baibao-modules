package com.baibao.option.service.impl;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.jws.WebService;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.option.repository.InfoStoreRepository;

import com.baibao.option.model.InfoStore;
import com.baibao.option.model.Option;
import com.baibao.option.service.InfoStoreManager;
import com.baibao.utils.Utils;
import com.baibao.utils.service.AbstractManagerImpl;
import jodd.util.CollectionUtil;

@WebService(serviceName = "infoStoreManager")
@Service("infoStoreManager")
public class InfoStoreManagerImpl
    extends AbstractManagerImpl<InfoStore, Long>
    implements InfoStoreManager
{
    @Autowired
    protected InfoStoreRepository infoStoreRepository;

    public void afterPropertiesSet() throws Exception {
        setRepository(infoStoreRepository);
    }

    @Transactional
	public void putStringValue(
		String objectType, String objectId, String name, String value)
	{
    	InfoStore is = getValue(objectType, objectId, name);
		if (is == null)
			is = new InfoStore();

		is.setObjectType(objectType);
		is.setObjectId(objectId);
		is.setName(name);
		is.setValue(value);

		infoStoreRepository.save(is);
	}

	public InfoStore getValue(String objectType, String objectId, String name)
	{
		ArrayList<InfoStore> iss =
			infoStoreRepository.getValue(objectType, objectId, name);
		if (CollectionUtils.isEmpty(iss))
			return null;

		return iss.get(0);
	}
	
	public String getStringValue(
		String objectType, String objectId, String name, String defaultValue)
	{
		InfoStore is = getValue(objectType, objectId, name);

		if (is == null)
			return defaultValue;

		return is.getValue();
	}

	public Long getLongValue(
		String objectType, String objectId, String name, Long defaultValue)
	{
		String strValue = getStringValue(objectType, objectId, name, null);

		if (strValue != null)
		{
			try
			{
				return Long.parseLong(strValue);
			} catch(Exception e)
			{ }
		}

		return defaultValue;
	}

	public Double getDoubleValue(
		String objectType, String objectId, String name, Double defaultValue)
	{
		String strValue = getStringValue(objectType, objectId, name, null);

		if (strValue != null)
		{
			try
			{
				return Double.parseDouble(strValue);
			} catch(Exception e)
			{ }
		}

		return defaultValue;
	}

	public Boolean getBooleanValue(
		String objectType, String objectId, String name, Boolean defaultValue)
	{
		String strValue = getStringValue(objectType, objectId, name, null);

		if (strValue != null)
		{
			try
			{
				if ("true".equalsIgnoreCase(strValue) ||
					"on".equalsIgnoreCase(strValue) ||
					"yes".equalsIgnoreCase(strValue))
					return true;

				if ("false".equalsIgnoreCase(strValue) ||
					"off".equalsIgnoreCase(strValue) ||
					"no".equalsIgnoreCase(strValue))
					return false;

				return Long.parseLong(strValue) != 0l;
			} catch(Exception e)
			{ }
		}

		return defaultValue;
	}

	public ArrayList<InfoStore> findInfo(String objectType, String objectId)
	{
		return infoStoreRepository.getInfo(objectType, objectId);
	}
}

