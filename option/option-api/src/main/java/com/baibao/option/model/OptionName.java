package com.baibao.option.model;

/**
 * 系统配置名称常量
 */
public class OptionName
{
	/**
	 * 逾期滞纳金的每日费率
	 */
	public static final String OVERDUERATE = "com.baibao.loan.overdue-rate";

	/**
	 * 逾期滞纳金的每日费率2
	 */
	public static final String OVERDUERATE2 = "com.baibao.loan.overdue-rate2";

	/**
	 * 逾期日阈值
	 */
	public static final String OVERDUEDAYS = "com.baibao.loan.overdue-days";
}
