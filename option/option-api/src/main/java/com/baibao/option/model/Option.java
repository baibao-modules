package com.baibao.option.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.openjpa.persistence.jdbc.Index;

/**
 * 系统配置信息
 */
@Entity @Table(name = "Property")
public class Option
	implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * 配置名称
	 */
	@Index
	@Column(length = 128)
	private String name;

	/**
	 * 配置值
	 */
	@Column(length = Integer.MAX_VALUE)
	private String value;

	/**
	 * 失效时间
	 */
	@Index
	private Long expired;
	
	/**
	 * 生效时间
	 */
	@Index
	private Long effected;

	/**
	 * 更新时间
	 */
	@Index
	private Timestamp updateTime;

	@Lob
	private String comment;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getExpired() {
		return expired;
	}

	public void setExpired(Long expired) {
		this.expired = expired;
	}

	public Long getEffected() {
		return effected;
	}

	public void setEffected(Long effected) {
		this.effected = effected;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
