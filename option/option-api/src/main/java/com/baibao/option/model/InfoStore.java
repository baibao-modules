
package com.baibao.option.model;

import javax.persistence.*;

import org.apache.openjpa.persistence.jdbc.Index;

@Entity
public class InfoStore
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
	 * 关联对象类型
	 */
	@Index
	@Column(length = 128)
	private String objectType;

	/**
	 * 关联对象id
	 */
	@Index
	@Column(length = 128)
	private String objectId;
    
	/**
	 * 配置名称
	 */
	@Index
	@Column(length = 128)
	private String name;

	/**
	 * 配置值
	 */
	@Column(length = Integer.MAX_VALUE)
	private String value;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

