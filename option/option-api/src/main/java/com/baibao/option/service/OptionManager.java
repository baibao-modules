package com.baibao.option.service;

import com.baibao.utils.service.GenericManager;

import java.util.ArrayList;
import javax.jws.WebService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.baibao.option.model.Option;

import javax.jws.WebMethod;

/**
 * 系统配置管理器
 */
@WebService
public interface OptionManager
	extends GenericManager<Option, Integer>
{
	/**
	 * 批量获取配置项
	 * 
	 * @param names 配置名称
	 * @return 配置项列表
	 */
	public ArrayList<Option> findOptionsByNames(String[] names);

	/**
	 * 设置系统配置项
	 * 
	 * @param name 配置名称
	 * @param value 配置值
	 * @return 配置信息
	 */
	@WebMethod(operationName = "setValue1")
	public Option setValue(String name, String value);
	
	/**
	 * 设置系统配置项
	 * 
	 * @param id 配置信息id
	 * @param value 配置值
	 * @return 配置信息
	 */
	@WebMethod(operationName = "setValue2")
	public Option setValue(Integer id, String value);

	/**
	 * 获取系统配置信息
	 * 
	 * @param name 配置名称
	 * @return 配置信息
	 */
	@WebMethod(operationName = "getValue1")
	public Option getValue(String name);
	
	/**
	 * 获取系统配置信息
	 * 
	 * @param name 配置名称
	 * @param defaultValue 默认值
	 * 
	 * @return 配置信息
	 */
	@WebMethod(operationName = "getValue2")
	public Option getValue(String name, String defaultValue);

	/**
	 * 以字符类型获取配置信息
	 * 
	 * @param name 配置名称
	 * @param defaultValue 默认值
	 * @return 配置值
	 */
	public String getStringValue(String name, String defaultValue);
	
	/**
	 * 以长整类型获取配置信息
	 * 
	 * @param name 配置名称
	 * @param defaultValue 默认值
	 * @return 配置值
	 */
	public Long getLongValue(String name, Long defaultValue);
	
	/**
	 * 以浮点类型获取配置信息
	 * 
	 * @param name 配置名称
	 * @param defaultValue 默认值
	 * @return 配置值
	 */
	public Double getDoubleValue(String name, Double defaultValue);
	
	/**
	 * 以布尔类型获取配置信息
	 * 
	 * @param name 配置名称
	 * @param defaultValue 默认值
	 * @return 配置值
	 */
	public Boolean getBooleanValue(String name, Boolean defaultValue);

	/**
	 * 向数据库添加一条配置项
	 * 
	 * @param name 配置信息
	 * @param value 配置值
	 * @param effected 生效时间
	 * @param expired 失效时间
	 * @return 配置信息
	 */
	public Option addValue(
		String name, String value, Long effected, Long expired);

	/**
	 * 删除指定配置信息
	 * 
	 * @param name 配置名称
	 */
	@WebMethod(operationName = "remove2")
	public void remove(String name);
}

