package com.baibao.option.service;

import javax.jws.WebService;

import com.baibao.option.model.InfoStore;

import java.util.ArrayList;

import javax.jws.WebMethod;

import com.baibao.utils.service.GenericManager;

@WebService
public interface InfoStoreManager
    extends GenericManager<InfoStore, Long>
{
	public ArrayList<InfoStore> findInfo(String objectType, String objectId);

	public void putStringValue(
		String objectType, String objectId, String name, String value);

	public String getStringValue(
		String objectType, String objectId, String name, String defaultValue);

	public Long getLongValue(
		String objectType, String objectId, String name, Long defaultValue);

	public Double getDoubleValue(
		String objectType, String objectId, String name, Double defaultValue);

	public Boolean getBooleanValue(
		String objectType, String objectId, String name, Boolean defaultValue);
}
