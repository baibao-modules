
package com.baibao.message.model;

import java.sql.Timestamp;

import javax.persistence.*;

import org.apache.openjpa.persistence.jdbc.Index;

@Table(name = "p2p_smsrecord")
@Entity
public class SmsRecord
{
    @Id
    @Column(name = "smsRecordId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Timestamp sendTime;

    @Column(name = "smsContent")
    private String content;

    @Column(name = "sendResult")
    private String result;
    
    private String realName;
    
    private String sendType;
    
    @Column(name = "failPhone")
    private String phone;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public Timestamp getSendTime() {
		return sendTime;
	}

	public void setSendTime(Timestamp sendTime) {
		this.sendTime = sendTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getSendType() {
		return sendType;
	}

	public void setSendType(String sendType) {
		this.sendType = sendType;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}

