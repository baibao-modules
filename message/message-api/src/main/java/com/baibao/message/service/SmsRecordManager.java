package com.baibao.message.service;

import javax.jws.WebService;

import java.util.Map;

import javax.jws.WebMethod;

import com.baibao.message.data.MessageType;
import com.baibao.message.model.SmsRecord;
import com.baibao.utils.service.GenericManager;

@WebService
public interface SmsRecordManager
    extends GenericManager<SmsRecord, Long>
{
	public SmsRecord sendSms(
		MessageType type, String phone,
		Map<String, Object> parameters);
}



