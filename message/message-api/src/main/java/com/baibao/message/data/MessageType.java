	
package com.baibao.message.data;

/**
 * 短信模板类型
 *
 */
public enum MessageType
{	
	/**
	 * 短信模板类型
	 */
	REGISTER("register", 1439129, "注册"),
	LOGINPASSWORD("retrievePassword", 1439131, "找回登录密码"),
	TRADINGPASSWORD("tradingPassword", 1439133, "找回交易密码"),
	LENDING("lending", 1486406, "放款"),
	FULLMARK("fullmark", 1486414, "满标"),
	REPAYMENT("repayment", 1486426, "还款"),
	INCOME("income", 1486418, "收益"),
	REPAY1("repay1", 1486420, "催收短信"),
	REPAY2("repay2", 1486422, "当日催收短信"),
	REPAY3("repay3", 1487102, "逾期催收短信");
	
	private int index;  //value
    private String name;   //key
    private String desc;   //描述
   
    private MessageType(String name, int index,String desc) {  
        this.name = name;  
        this.index = index;  
        this.desc =desc;
    } 
    /**
	 * 通过value获取对象	 
	 */  
	public static MessageType getMessageState(int index) {  
        for (MessageType c : MessageType.values()) {  
        	 if (c.getIndex()== index) {   
                return c;  
            }  
        }  
        return null;  
    }       
    
    public String getName() {  
        return name;  
    }  
    public void setName(String name) {  
        this.name = name;  
    }  
    public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
