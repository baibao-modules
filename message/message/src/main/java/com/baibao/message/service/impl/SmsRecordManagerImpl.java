package com.baibao.message.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.jws.WebService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.message.data.MessageType;
import com.baibao.message.model.SmsRecord;
import com.baibao.message.repository.SmsRecordRepository;
import com.baibao.message.service.SmsRecordManager;
import com.baibao.utils.HttpHelper;
import com.baibao.utils.JsonHelper;
import com.baibao.utils.Utils;
import com.baibao.utils.service.AbstractManagerImpl;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@WebService(serviceName = "smsRecordManager")
@Service("smsRecordManager")
public class SmsRecordManagerImpl
    extends AbstractManagerImpl<SmsRecord, Long>
    implements SmsRecordManager
{
	private static Logger log =
		LoggerFactory.getLogger(SmsRecordManagerImpl.class);
	
    @Autowired
    protected SmsRecordRepository smsRecordRepository;

    @Value("${SMSAPIKEY}")
    private String smsKey;
    
    public void afterPropertiesSet() throws Exception {
        setRepository(smsRecordRepository);
    }

	@Override
	@Transactional
	public SmsRecord sendSms(
		MessageType type, String phone,
		Map<String, Object> parameters)
	{
		StringBuffer params = new StringBuffer();
		
		boolean first = false;
		
		for(Map.Entry<String, Object> entry: parameters.entrySet())
		{
			if (first)
			{
				params.append("&");
			}
			
			params.append(entry.getKey())
				.append("=").append(entry.getValue());

			first = true;
		}
		
		log.info(String.format(
			"发送to%s模板短信: %s!%s", phone, type, params));
		String url = "http://yunpian.com/v1/sms/tpl_send.json";
		
		Map<String, String> paramMap =
			new HashMap<String, String>();
		
		paramMap.put("apikey", smsKey);
		paramMap.put("mobile", phone);
		paramMap.put("tpl_id", String.valueOf(type.getIndex()));
		paramMap.put("tpl_value", params.toString());

		int code = -1;
		String message = "";
		String value = "";

		try
		{
			value = HttpHelper.sendPost(url, paramMap);
	
			JsonObject element =
				(JsonObject) JsonHelper.parseJson(value);
	
			code = element.get("code").getAsInt();
			message =
				element.has("msg")?
					element.get("msg").getAsString(): "";
		} catch(Exception e)
		{
			code = -1;
			message = e.toString();
		}

		SmsRecord record = new SmsRecord();

		record.setSendType(String.valueOf(type));
		record.setContent(params.toString());
		record.setResult(value);
		record.setSendTime(Utils.now());
		record.setPhone(phone);
		record.setRealName(phone);
		record.setResult(code == 0? "发送成功": message);

		smsRecordRepository.save(record);
		
		return record;
	}
}



