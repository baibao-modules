package com.baibao.message.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.baibao.message.model.SmsRecord;

@Repository
public interface SmsRecordRepository
    extends JpaRepository<SmsRecord, Long>, JpaSpecificationExecutor<SmsRecord>
{
}



