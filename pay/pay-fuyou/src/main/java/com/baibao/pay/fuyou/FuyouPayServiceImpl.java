package com.baibao.pay.fuyou;

import java.sql.Timestamp;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baibao.pay.BussStatus;
import com.baibao.pay.BussType;
import com.baibao.pay.GetUniqueNoService;
import com.baibao.pay.PayLoggerService;
import com.baibao.pay.PayService;
import com.baibao.pay.data.AccountInfoRequest;
import com.baibao.pay.data.AccountInfoResponse;
import com.baibao.pay.data.ActiveAccountRequest;
import com.baibao.pay.data.ActiveAccountResponse;
import com.baibao.pay.data.BalanceRequest;
import com.baibao.pay.data.BalanceResponse;
import com.baibao.pay.data.BankCard;
import com.baibao.pay.data.DepositRequest;
import com.baibao.pay.data.DepositResponse;
import com.baibao.pay.data.FreezeRequest;
import com.baibao.pay.data.FreezeResponse;
import com.baibao.pay.data.HostingCollectRequest;
import com.baibao.pay.data.HostingCollectResponse;
import com.baibao.pay.data.HostingPayRequest;
import com.baibao.pay.data.HostingPayResponse;
import com.baibao.pay.data.HostingRefundRequest;
import com.baibao.pay.data.HostingRefundResponse;
import com.baibao.pay.data.ModifyAccountRequest;
import com.baibao.pay.data.ModifyAccountResponse;
import com.baibao.pay.data.PayResponse;
import com.baibao.pay.data.QueryDepositRequest;
import com.baibao.pay.data.QueryDepositResponse;
import com.baibao.pay.data.QueryDetailRequest;
import com.baibao.pay.data.QueryDetailResponse;
import com.baibao.pay.data.QueryDetailResponse.DetailRecord;
import com.baibao.pay.data.QueryHostingTradeRequest;
import com.baibao.pay.data.QueryHostingTradeResponse;
import com.baibao.pay.data.QueryWithdrawRequest;
import com.baibao.pay.data.QueryWithdrawResponse;
import com.baibao.pay.data.RedirectRequest;
import com.baibao.pay.data.RedirectResponse;
import com.baibao.pay.data.QueryWithdrawResponse.WithdrawRecord;
import com.baibao.utils.HttpHelper;
import com.baibao.utils.JsonHelper;
import com.baibao.utils.MapBuilder;
import com.baibao.utils.Utils;
import com.baibao.pay.data.UnfreezeRequest;
import com.baibao.pay.data.UnfreezeResponse;
import com.baibao.pay.data.WithdrawRequest;
import com.baibao.pay.data.WithdrawResponse;
import com.baibao.pay.data.QueryDepositResponse.DepositRecord;
import com.baibao.pay.data.QueryHostingTradeResponse.HostingTradeRecord;
import com.fuiou.data.CommonRspData;
import com.fuiou.data.FreezeReqData;
import com.fuiou.data.ModifyUserInfReqData;
import com.fuiou.data.QueryBalanceReqData;
import com.fuiou.data.QueryBalanceResultData;
import com.fuiou.data.QueryBalanceRspData;
import com.fuiou.data.QueryCzTxReq;
import com.fuiou.data.QueryCzTxRspData;
import com.fuiou.data.QueryCzTxRspDetailData;
import com.fuiou.data.QueryDetail;
import com.fuiou.data.QueryOpResultSet;
import com.fuiou.data.QueryReqData;
import com.fuiou.data.QueryRspData;
import com.fuiou.data.QueryTxnReqData;
import com.fuiou.data.QueryTxnRspData;
import com.fuiou.data.QueryTxnRspDetailData;
import com.fuiou.data.QueryUserInfsReqData;
import com.fuiou.data.QueryUserInfsRspData;
import com.fuiou.data.QueryUserInfsRspDetailData;
import com.fuiou.data.RegReqData;
import com.fuiou.data.TransferBmuReqData;
import com.fuiou.data.UnFreezeRspData;
import com.fuiou.service.FuiouService;
import com.fuiou.util.SecurityUtils;
import com.fuiou.util.Util;

public class FuyouPayServiceImpl
	implements PayService, InitializingBean
{
	private static Logger log = LoggerFactory.getLogger(FuyouPayServiceImpl.class);

	@Value("${fuyou-url}")
	protected String fuyouUrl;

	@Value("${callback-url}")
	protected String callbackUrl;

	@Value("${fuyou-merchantid}")
	protected String merchantid;

	@Value("${fuyou-company-account}")
	protected String companyAccountId;
	
	@Value("${fuyou-receivable-account}")
	protected String receivableAccountId;
	
	@Autowired
	protected GetUniqueNoService getUniqueNoService;

	@Autowired(required = false)
	protected PayLoggerService payLoggerService;

	protected SimpleDateFormat formater =
		new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	protected SimpleDateFormat dateFormater =
		new SimpleDateFormat("yyyy-MM-dd");
	protected SimpleDateFormat shortDateFormater =
		new SimpleDateFormat("yyyyMMdd");
	
	protected SimpleDateFormat parser =
		new SimpleDateFormat("yyyyMMddHHmmss");
	
	protected static final String channel = "fuiou";
	
	@Override
	public ActiveAccountResponse activeAccount(ActiveAccountRequest request)
	{
		ActiveAccountResponse response = new ActiveAccountResponse();

		RegReqData reqData = new RegReqData();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getActiveAccountFlow());

		reqData.setMchnt_cd(merchantid); //商户代码
		reqData.setMchnt_txn_ssn(request.getTradeNo()); //流水号
		reqData.setCust_nm(request.getCustomName()); //真实姓名
		reqData.setCertif_id(request.getCertId()); //身份证号
		reqData.setMobile_no(request.getPhone()); //用户电话
		
		if (StringUtils.isNotBlank(request.getEmail()))
			reqData.setEmail(request.getEmail());
		
		//开户行地区代码
		reqData.setCity_id(request.getBankCard().getCityId());
		//开户行行别代码
		reqData.setParent_bank_id(request.getBankCard().getBankId());
		
		reqData.setBank_nm(request.getBankCard().getBankName()); //开户支行
		reqData.setCapAcntNo(request.getBankCard().getCardNumber());//银行卡号
		reqData.setRem(request.getRem());

		response.setTradeNo(request.getTradeNo());

		try
		{
			payLoggerService.log(
				Utils.now(), request.getTradeNo(), BussType.bind, null, channel,
				request.getPhone(), null,
				JsonHelper.formatJson(reqData), null,
				BussStatus.pending);

			CommonRspData rspData = pack(FuiouService.reg(reqData), response);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				JsonHelper.formatJson(rspData),
				response.isOk()? BussStatus.success: BussStatus.failed);
		} catch(Throwable e)
		{
			log.info("failed to fuiou", e);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				e.toString(), BussStatus.failed);

			pack(e, response);
		}

		if (response.getStatus() == 5343)
		{//已开户的话，就修改
			ModifyAccountRequest modifyRequest =
				JsonHelper.convert(request, ModifyAccountRequest.class);

			ModifyAccountResponse modifyResponse = modifyAccount(modifyRequest);
			
			response = JsonHelper.convert(
				modifyResponse, ActiveAccountResponse.class);

			response.setStatus(PayResponse.OK);
		}

		response.setAccountId(request.getPhone());

		return response;
	}

	@Override
	public ModifyAccountResponse modifyAccount(ModifyAccountRequest request)
	{
		ModifyAccountResponse response = new ModifyAccountResponse();
		
		ModifyUserInfReqData reqData = new ModifyUserInfReqData();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getModifyAccountFlow());

		reqData.setMchnt_cd(merchantid); //商户代码
		reqData.setMchnt_txn_ssn(request.getTradeNo()); //流水号
		reqData.setCust_nm(request.getCustomName()); //真实姓名
		reqData.setCertif_id(request.getCertId()); //身份证号
		reqData.setMobile_no(request.getPhone()); //用户电话

		if (StringUtils.isNotBlank(request.getEmail()))
			reqData.setEmail(request.getEmail());
		
		//开户行地区代码
		reqData.setCity_id(request.getBankCard().getCityId());
		//开户行行别代码
		reqData.setParent_bank_id(request.getBankCard().getBankId());
		
		reqData.setBank_nm(request.getBankCard().getBankName()); //开户支行
		reqData.setCapAcntNo(request.getBankCard().getCardNumber());//银行卡号

		response.setTradeNo(request.getTradeNo());

		try
		{
			payLoggerService.log(
				Utils.now(), request.getTradeNo(), BussType.bind, "modify", channel,
				request.getPhone(), null,
				JsonHelper.formatJson(reqData), null,
				BussStatus.pending);

			CommonRspData rspData = pack(FuiouService.modifyUserInf(reqData), response);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				JsonHelper.formatJson(rspData),
				response.isOk()? BussStatus.success: BussStatus.failed);
		} catch(Throwable e)
		{
			log.info("failed to fuiou", e);

			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				e.toString(), BussStatus.failed);

			pack(e, response);
		}

		response.setStatus(PayResponse.OK);
		
		return response;
	}

	@Override
	public AccountInfoResponse getAccountInfo(AccountInfoRequest request)
	{
		AccountInfoResponse response = new AccountInfoResponse();

		QueryUserInfsReqData reqData = new QueryUserInfsReqData();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getQueryAccountFlow());

		reqData.setMchnt_cd(merchantid);
		reqData.setMchnt_txn_ssn(request.getTradeNo());
		reqData.setMchnt_txn_dt(shortDateFormater.format(Utils.now()));
		reqData.setUser_ids(request.getAccountId());

		response.setTradeNo(request.getTradeNo());

		try
		{
			payLoggerService.log(
				Utils.now(), request.getTradeNo(),
				BussType.custom, "query-balance", channel,
				request.getAccountId(), null,
				JsonHelper.formatJson(reqData), null,
				BussStatus.pending);

			QueryUserInfsRspData rspData = FuiouService.queryUserInfs(reqData);
			
			response.setStatus(Integer.parseInt(rspData.getResp_code()));
			if (response.isOk() &&
				CollectionUtils.isNotEmpty(rspData.getResults()))
			{
				QueryUserInfsRspDetailData detail = rspData.getResults().get(0);
	
				response.setAccountId(request.getAccountId());
				response.setCustomName(detail.getCust_nm());
				response.setCertId(detail.getCertif_id());
				response.setPhone(detail.getMobile_no());
				response.setEmail(detail.getEmail());
				
				BankCard bankCard = new BankCard();
				
				bankCard.setCityId(detail.getCity_id());
				bankCard.setBankId(detail.getParent_bank_id());
				bankCard.setBankName(detail.getBank_nm());
				bankCard.setCardNumber(detail.getCapAcntNo());
				
				response.setBankCard(bankCard);
			}
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				JsonHelper.formatJson(rspData),
				response.isOk()? BussStatus.success: BussStatus.failed);
		} catch(Throwable e)
		{
			log.info("failed to fuiou", e);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				e.toString(), BussStatus.failed);

			pack(e, response);
		}
		
		return response;
	}

	@Override
	public FreezeResponse freeze(FreezeRequest request)
	{
		FreezeResponse response = new FreezeResponse();
		
		FreezeReqData reqData = new FreezeReqData();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getFreezeFlow());
		
		reqData.setMchnt_cd(merchantid);
		reqData.setMchnt_txn_ssn(request.getTradeNo());
		reqData.setCust_no(request.getAccountId());
		reqData.setAmt(String.valueOf(request.getAmount()));
		reqData.setRem(request.getRem());

		response.setTradeNo(request.getTradeNo());
		
		try
		{
			payLoggerService.log(
				Utils.now(), request.getTradeNo(),
				BussType.custom, "freeze", channel,
				request.getAccountId(), null,
				JsonHelper.formatJson(reqData), null,
				BussStatus.pending);
		
			CommonRspData rspData = pack(FuiouService.freeze(reqData), response);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				JsonHelper.formatJson(rspData),
				response.isOk()? BussStatus.success: BussStatus.failed);
		} catch(Throwable e)
		{
			log.info("failed to fuiou", e);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				e.toString(), BussStatus.failed);

			pack(e, response);
		}
		
		return response;
	}

	@Override
	public UnfreezeResponse unfreeze(UnfreezeRequest request)
	{
		UnfreezeResponse response = new UnfreezeResponse();
		
		FreezeReqData reqData = new FreezeReqData();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getUnfreezeFlow());

		reqData.setMchnt_cd(merchantid);
		reqData.setMchnt_txn_ssn(request.getTradeNo());
		reqData.setCust_no(request.getAccountId());
		reqData.setAmt(String.valueOf(request.getAmount()));
		reqData.setRem(request.getRem());

		response.setTradeNo(request.getTradeNo());
		
		try
		{
			payLoggerService.log(
				Utils.now(), request.getTradeNo(),
				BussType.custom, "unfreeze", channel,
				request.getAccountId(), null,
				JsonHelper.formatJson(reqData), null,
				BussStatus.pending);
		
			UnFreezeRspData rspData = FuiouService.unFreeze(reqData);
			
			response.setStatus(Integer.parseInt(rspData.getResp_code()));
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				JsonHelper.formatJson(rspData),
				response.isOk()? BussStatus.success: BussStatus.failed);
		} catch(Throwable e)
		{
			log.info("failed to fuiou", e);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				e.toString(), BussStatus.failed);

			pack(e, response);
		}

		return response;
	}

	@Override
	public HostingCollectResponse hostingCollect(HostingCollectRequest request)
	{
		HostingCollectResponse response = new HostingCollectResponse();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getActivityTransFlow());

		if (request.getReason() == HostingCollectRequest.Reason.common ||
			request.getReason() == HostingCollectRequest.Reason.invest)
		{//直接划向金帐号
			TransferBmuReqData reqData = new TransferBmuReqData();

			reqData.setMchnt_cd(merchantid);
			reqData.setMchnt_txn_ssn(request.getTradeNo());
			reqData.setOut_cust_no(request.getPayAccountId());
			reqData.setIn_cust_no(receivableAccountId);
			reqData.setAmt(String.valueOf(request.getAmount()));
			reqData.setRem(request.getRem());

			response.setTradeNo(request.getTradeNo());
			
			try
			{
				payLoggerService.log(
					Utils.now(), request.getTradeNo(),
					BussType.collecting, String.valueOf(request.getReason()),
					channel,
					request.getPayAccountId(), null,
					JsonHelper.formatJson(reqData), null,
					BussStatus.pending);

				CommonRspData rspData = pack(FuiouService.transferBu(reqData), response);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					JsonHelper.formatJson(rspData),
					response.isOk()? BussStatus.success: BussStatus.failed);
			} catch(Throwable e)
			{
				log.info("failed to fuiou", e);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					e.toString(), BussStatus.failed);

				pack(e, response);
			}
		} else
		if (request.getReason() == HostingCollectRequest.Reason.repay ||
			request.getReason() == HostingCollectRequest.Reason.interFee)
		{//直接划向公司帐号
			TransferBmuReqData reqData = new TransferBmuReqData();

			reqData.setMchnt_cd(merchantid);
			reqData.setMchnt_txn_ssn(request.getTradeNo());
			reqData.setOut_cust_no(request.getPayAccountId());
			reqData.setIn_cust_no(companyAccountId);
			reqData.setAmt(String.valueOf(request.getAmount()));
			reqData.setRem(request.getRem());

			if (request.getReason() == HostingCollectRequest.Reason.interFee)
			{
				reqData.setOut_cust_no(receivableAccountId);
			}
			
			response.setTradeNo(request.getTradeNo());

			try
			{
				payLoggerService.log(
					Utils.now(), request.getTradeNo(),
					BussType.collecting, String.valueOf(request.getReason()),
					channel,
					request.getPayAccountId(), null,
					JsonHelper.formatJson(reqData), null,
					BussStatus.pending);

				CommonRspData rspData = pack(FuiouService.transferBmu(reqData), response);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					JsonHelper.formatJson(rspData),
					response.isOk()? BussStatus.success: BussStatus.failed);
			} catch(Throwable e)
			{
				log.info("failed to fuiou", e);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					e.toString(), BussStatus.failed);

				pack(e, response);
			}
		}

		return response;
	}

	@Override
	public HostingPayResponse hostingPay(HostingPayRequest request)
	{
		HostingPayResponse response = new HostingPayResponse();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getActivityTransFlow2());

		if (request.getReason() == HostingPayRequest.Reason.income)
		{//从公司帐号划向个人
			TransferBmuReqData reqData = new TransferBmuReqData();

			reqData.setMchnt_cd(merchantid);
			reqData.setMchnt_txn_ssn(request.getTradeNo());
			reqData.setOut_cust_no(companyAccountId);
			reqData.setIn_cust_no(request.getPayeeAccountId());
			reqData.setAmt(String.valueOf(request.getAmount()));
			reqData.setRem(request.getRem());

			response.setTradeNo(request.getTradeNo());
			
			try
			{
				payLoggerService.log(
					Utils.now(), request.getTradeNo(),
					BussType.paying, String.valueOf(request.getReason()),
					channel,
					request.getPayeeAccountId(), null,
					JsonHelper.formatJson(reqData), null,
					BussStatus.pending);

				CommonRspData rspData = pack(FuiouService.transferBmu(reqData), response);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					JsonHelper.formatJson(rspData),
					response.isOk()? BussStatus.success: BussStatus.failed);
			} catch(Throwable e)
			{
				log.info("failed to fuiou", e);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					e.toString(), BussStatus.failed);

				pack(e, response);
			}
		} else
		if (request.getReason() == HostingPayRequest.Reason.common ||
			request.getReason() == HostingPayRequest.Reason.lending)
		{//从金帐号划向个人
			TransferBmuReqData reqData = new TransferBmuReqData();

			reqData.setMchnt_cd(merchantid);
			reqData.setMchnt_txn_ssn(request.getTradeNo());
			reqData.setOut_cust_no(receivableAccountId);
			reqData.setIn_cust_no(request.getPayeeAccountId());
			reqData.setAmt(String.valueOf(request.getAmount()));
			reqData.setRem(request.getRem());

			response.setTradeNo(request.getTradeNo());

			try
			{
				payLoggerService.log(
					Utils.now(), request.getTradeNo(),
					BussType.paying, String.valueOf(request.getReason()),
					channel,
					request.getPayeeAccountId(), null,
					JsonHelper.formatJson(reqData), null,
					BussStatus.pending);

				CommonRspData rspData = pack(FuiouService.transferBu(reqData), response);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					JsonHelper.formatJson(rspData),
					response.isOk()? BussStatus.success: BussStatus.failed);
			} catch(Throwable e)
			{
				log.info("failed to fuiou", e);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					e.toString(), BussStatus.failed);

				pack(e, response);
			}
		}

		return response;
	}

	@Override
	public HostingRefundResponse hostingRefund(HostingRefundRequest request)
	{
		HostingRefundResponse response = new HostingRefundResponse();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getCommTransFlow());

		if (request.getReason() == HostingRefundRequest.Reason.cancelMark)
		{//从金帐号向个人
			TransferBmuReqData reqData = new TransferBmuReqData();

			reqData.setMchnt_cd(merchantid);
			reqData.setMchnt_txn_ssn(request.getTradeNo());
			reqData.setOut_cust_no(receivableAccountId);
			reqData.setIn_cust_no(request.getAccountId());
			reqData.setAmt(String.valueOf(request.getAmount()));
			reqData.setRem(request.getRem());

			response.setTradeNo(request.getTradeNo());
			
			try
			{
				payLoggerService.log(
					Utils.now(), request.getTradeNo(),
					BussType.refund, String.valueOf(request.getReason()),
					channel,
					request.getAccountId(), null,
					JsonHelper.formatJson(reqData), null,
					BussStatus.pending);
			
				CommonRspData rspData = pack(FuiouService.transferBu(reqData), response);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					JsonHelper.formatJson(rspData),
					response.isOk()? BussStatus.success: BussStatus.failed);
			} catch(Throwable e)
			{
				log.info("failed to fuiou", e);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					e.toString(), BussStatus.failed);

				pack(e, response);
			}
		} else
		if (request.getReason() == HostingRefundRequest.Reason.common)
		{
			TransferBmuReqData reqData = new TransferBmuReqData();

			reqData.setMchnt_cd(merchantid);
			reqData.setMchnt_txn_ssn(request.getTradeNo());
			reqData.setOut_cust_no(companyAccountId);
			reqData.setIn_cust_no(request.getAccountId());
			reqData.setAmt(String.valueOf(request.getAmount()));
			reqData.setRem(request.getRem());

			response.setTradeNo(request.getTradeNo());
			
			try
			{
				payLoggerService.log(
					Utils.now(), request.getTradeNo(),
					BussType.refund, String.valueOf(request.getReason()),
					channel,
					request.getAccountId(), null,
					JsonHelper.formatJson(reqData), null,
					BussStatus.pending);
			
				CommonRspData rspData = pack(FuiouService.transferBmu(reqData), response);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					JsonHelper.formatJson(rspData),
					response.isOk()? BussStatus.success: BussStatus.failed);
			} catch(Throwable e)
			{
				log.info("failed to fuiou", e);
				
				payLoggerService.logback(
					Utils.now(), request.getTradeNo(),
					e.toString(), BussStatus.failed);

				pack(e, response);
			}
		}

		return response;
	}

	@Override
	public DepositResponse deposit(DepositRequest request)
	{
		DepositResponse response = new DepositResponse();

		String callback = String.format("%s/depositNofity.fn", callbackUrl);
		
		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getPaymentTransFlow());

		response.setTradeNo(request.getTradeNo());

		if (request.getReason() == DepositRequest.Reason.app)
		{
			response.setTargetUrl(
				String.format("%s/app/500001.action", fuyouUrl));
		} else
		{
			response.setTargetUrl(
				String.format("%s/500001.action", fuyouUrl));
		}

		response.setTargetMethod("post");
		response.setTargetType("application/x-www-form-urlencoded");
		response.setBody(new MapBuilder<String, Object>()
			.append("mchnt_cd", merchantid)
			.append("mchnt_txn_ssn", request.getTradeNo())
			.append("login_id", request.getAccountId())
			.append("amt", request.getAmount())
			.append("page_notify_url", request.getNotifyUrl())
			.append("back_notify_url", callback)
			.append("signature",
				SecurityUtils.sign(Utils.joinStr(
					new String[] {
						String.valueOf(request.getAmount()), callback,
						request.getAccountId(),
						merchantid, request.getTradeNo(),
						request.getNotifyUrl()
					}, "|")))
			.toMap());
		response.setTargetBody(HttpHelper.encodeForm(response.getBody()));


		payLoggerService.log(
			Utils.now(), request.getTradeNo(),
			BussType.recharge, "deposit",
			channel,
			request.getAccountId(), null,
			JsonHelper.formatJson(response), null,
			BussStatus.pending);
		
		
		return response;
	}

	@Override
	public DepositResponse recharge(DepositRequest request)
	{
		DepositResponse response = new DepositResponse();

		String callback = String.format("%s/rechargeNofity.fn", callbackUrl);

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getPaymentTransFlow());

		response.setTradeNo(request.getTradeNo());

		if (request.getReason() == DepositRequest.Reason.app)
		{
			response.setTargetUrl(
				String.format("%s/app/500002.action", fuyouUrl));
		} else
		{
			response.setTargetUrl(
				String.format("%s/500002.action", fuyouUrl));
		}

		response.setTargetMethod("post");
		response.setTargetType("application/x-www-form-urlencoded");
		response.setBody(new MapBuilder<String, Object>()
			.append("mchnt_cd", merchantid)
			.append("mchnt_txn_ssn", request.getTradeNo())
			.append("login_id", request.getAccountId())
			.append("amt", request.getAmount())
			.append("page_notify_url", request.getNotifyUrl())
			.append("back_notify_url", callback)
			.append("signature",
				SecurityUtils.sign(Utils.joinStr(
					new String[] {
						String.valueOf(request.getAmount()), callback,
						request.getAccountId(),
						merchantid, request.getTradeNo(),
						request.getNotifyUrl()
					}, "|")))
			.toMap());
		response.setTargetBody(HttpHelper.encodeForm(response.getBody()));

		payLoggerService.log(
			Utils.now(), request.getTradeNo(),
			BussType.recharge, "recharge",
			channel,
			request.getAccountId(), null,
			JsonHelper.formatJson(response), null,
			BussStatus.pending);
		
		return response;
	}

	@Override
	public WithdrawResponse withdraw(WithdrawRequest request) 
	{
		WithdrawResponse response = new WithdrawResponse();

		String callback = String.format("%s/withdrawNofity.fn", callbackUrl);

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getWithDrawTransFlow());

		response.setTradeNo(request.getTradeNo());

		if (request.getReason() == WithdrawRequest.Reason.app)
		{
			response.setTargetUrl(
				String.format("%s/app/500003.action", fuyouUrl));
		} else
		{
			response.setTargetUrl(
				String.format("%s/500003.action", fuyouUrl));
		}

		response.setTargetMethod("post");
		response.setTargetType("application/x-www-form-urlencoded");
		response.setBody(new MapBuilder<String, Object>()
			.append("mchnt_cd", merchantid)
			.append("mchnt_txn_ssn", request.getTradeNo())
			.append("login_id", request.getAccountId())
			.append("amt", request.getAmount())
			.append("page_notify_url", request.getNotifyUrl())
			.append("back_notify_url", callback)
			.append("signature",
				SecurityUtils.sign(Utils.joinStr(
					new String[] {
						String.valueOf(request.getAmount()), callback,
						request.getAccountId(),
						merchantid, request.getTradeNo(),
						request.getNotifyUrl()
					}, "|")))
			.toMap());
		response.setTargetBody(HttpHelper.encodeForm(response.getBody()));

		payLoggerService.log(
			Utils.now(), request.getTradeNo(),
			BussType.withdraw, null,
			channel,
			request.getAccountId(), null,
			JsonHelper.formatJson(response), null,
			BussStatus.pending);
		
		return response;
	}

	@Override
	public QueryDepositResponse queryDeposit(QueryDepositRequest request) 
	{
		QueryDepositResponse response = new QueryDepositResponse();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getCommTransFlow());

		QueryCzTxReq reqData = new QueryCzTxReq();

		reqData.setMchnt_cd(merchantid);
		reqData.setMchnt_txn_ssn(request.getTradeNo());

		reqData.setBusi_tp("PW11");

		reqData.setStart_time(formater.format(request.getStartTime()));
		reqData.setEnd_time(formater.format(request.getEndTime()));
		
		reqData.setPage_no(String.valueOf(request.getPage()));
		reqData.setPage_size(String.valueOf(request.getSize()));

		if (StringUtils.isEmpty(request.getAccountId()))
			reqData.setCust_no(request.getAccountId());

		response.setTradeNo(request.getTradeNo());
		response.setPage(request.getPage());
		response.setSize(request.getSize());

		try
		{
			payLoggerService.log(
				Utils.now(), request.getTradeNo(),
				BussType.custom, "query-deposit",
				channel,
				request.getAccountId(), null,
				JsonHelper.formatJson(reqData), null,
				BussStatus.pending);
			
			QueryCzTxRspData rspData = FuiouService.querycztx(reqData);

			response.setStatus(Integer.parseInt(rspData.getResp_code()));

			response.setTotalElements(
				Integer.parseInt(rspData.getTotal_number()));
			if (CollectionUtils.isNotEmpty(rspData.getResults()))
			{
				ArrayList<DepositRecord> records =
					new ArrayList<DepositRecord>(rspData.getResults().size());
				response.setResult(records);
				
				for(QueryCzTxRspDetailData detail: rspData.getResults())
				{
					DepositRecord record = new DepositRecord();

					record.setDatetime(new Timestamp(
						parser.parse(
							detail.getTxn_date() + detail.getTxn_time(),
							new ParsePosition(0)).getTime()));
					record.setTradeNo(detail.getMchnt_ssn());
					record.setAccountId(detail.getCust_no());
					record.setAccountName(detail.getArtif_nm());
					record.setAmount(Long.parseLong(detail.getTxn_amt()));
					record.setRem(detail.getRemark());

					record.setSuccessed(
						Integer.parseInt(detail.getTxn_rsp_cd()) == 0);
					record.setMessage(detail.getRsp_cd_desc());

					records.add(record);
				}
			}
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				JsonHelper.formatJson(rspData),
				response.isOk()? BussStatus.success: BussStatus.failed);
		} catch(Throwable e)
		{
			log.info("failed to fuiou", e);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				e.toString(), BussStatus.failed);

			pack(e, response);
		}

		return response;
	}

	@Override
	public QueryWithdrawResponse queryWithdraw(QueryWithdrawRequest request) 
	{
		QueryWithdrawResponse response = new QueryWithdrawResponse();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getCommTransFlow());

		QueryCzTxReq reqData = new QueryCzTxReq();

		reqData.setMchnt_cd(merchantid);
		reqData.setMchnt_txn_ssn(request.getTradeNo());

		reqData.setBusi_tp("PWTX");

		reqData.setStart_time(formater.format(request.getStartTime()));
		reqData.setEnd_time(formater.format(request.getEndTime()));
		
		reqData.setPage_no(String.valueOf(request.getPage()));
		reqData.setPage_size(String.valueOf(request.getSize()));

		if (StringUtils.isEmpty(request.getAccountId()))
			reqData.setCust_no(request.getAccountId());

		response.setTradeNo(request.getTradeNo());
		response.setPage(request.getPage());
		response.setSize(request.getSize());

		try
		{
			payLoggerService.log(
				Utils.now(), request.getTradeNo(),
				BussType.custom, "query-withdraw",
				channel,
				request.getAccountId(), null,
				JsonHelper.formatJson(reqData), null,
				BussStatus.pending);
		
			QueryCzTxRspData rspData = FuiouService.querycztx(reqData);

			response.setStatus(Integer.parseInt(rspData.getResp_code()));

			response.setTotalElements(
				Integer.parseInt(rspData.getTotal_number()));
			if (CollectionUtils.isNotEmpty(rspData.getResults()))
			{
				ArrayList<WithdrawRecord> records =
					new ArrayList<WithdrawRecord>(rspData.getResults().size());
				response.setResult(records);
				
				for(QueryCzTxRspDetailData detail: rspData.getResults())
				{
					WithdrawRecord record = new WithdrawRecord();

					record.setDatetime(new Timestamp(
						parser.parse(
							detail.getTxn_date() + detail.getTxn_time(),
							new ParsePosition(0)).getTime()));
					record.setTradeNo(detail.getMchnt_ssn());
					record.setAccountId(detail.getCust_no());
					record.setAccountName(detail.getArtif_nm());
					record.setAmount(Long.parseLong(detail.getTxn_amt()));
					record.setRem(detail.getRemark());

					record.setSuccessed(
						Integer.parseInt(detail.getTxn_rsp_cd()) == 0);
					record.setMessage(detail.getRsp_cd_desc());

					records.add(record);
				}
			}
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				JsonHelper.formatJson(rspData),
				response.isOk()? BussStatus.success: BussStatus.failed);
		} catch(Throwable e)
		{
			log.info("failed to fuiou", e);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				e.toString(), BussStatus.failed);

			pack(e, response);
		}

		return response;
	}

	@Override
	public QueryHostingTradeResponse
		queryHostingTrade(QueryHostingTradeRequest request) 
	{
		QueryHostingTradeResponse response = new QueryHostingTradeResponse();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getCommTransFlow());

		QueryTxnReqData reqData = new QueryTxnReqData();
		
		reqData.setMchnt_cd(merchantid);
		reqData.setMchnt_txn_ssn(request.getTradeNo());

		reqData.setBusi_tp("PWTX");

		reqData.setStart_day(dateFormater.format(request.getStartTime()));
		reqData.setEnd_day(dateFormater.format(request.getEndTime()));

		reqData.setPage_no(String.valueOf(request.getPage()));
		reqData.setPage_size(String.valueOf(request.getSize()));

		if (StringUtils.isEmpty(request.getAccountId()))
			reqData.setCust_no(request.getAccountId());

		response.setTradeNo(request.getTradeNo());
		response.setPage(request.getPage());
		response.setSize(request.getSize());

		try
		{
			payLoggerService.log(
				Utils.now(), request.getTradeNo(),
				BussType.custom, "query-hostingtrade",
				channel,
				request.getAccountId(), null,
				JsonHelper.formatJson(reqData), null,
				BussStatus.pending);

			QueryTxnRspData rspData = FuiouService.queryTxn(reqData);

			response.setStatus(Integer.parseInt(rspData.getResp_code()));

			response.setTotalElements(
				Integer.parseInt(rspData.getTotal_number()));
			if (CollectionUtils.isNotEmpty(rspData.getResults()))
			{
				ArrayList<HostingTradeRecord> records =
					new ArrayList<HostingTradeRecord>(
						rspData.getResults().size());
				response.setResult(records);

				for(QueryTxnRspDetailData detail: rspData.getResults())
				{
					HostingTradeRecord record = new HostingTradeRecord();

					record.setTimestamp(new Timestamp(
						parser.parse(
							detail.getTxn_date() + detail.getTxn_time(),
							new ParsePosition(0)).getTime()));
					record.setTradeNo(detail.getMchnt_ssn());
					record.setAccountId(detail.getOut_cust_no());
					record.setAccountName(detail.getOut_artif_nm());
					record.setInAccountId(detail.getIn_cust_no());
					record.setInAccountName(detail.getIn_artif_nm());
					record.setAmount(Long.parseLong(detail.getTxn_amt()));

					record.setSuccessed(
						Integer.parseInt(detail.getTxn_rsp_cd()) == 0);
					record.setMessage(detail.getRsp_cd_desc());
					
					records.add(record);
				}
			}
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				JsonHelper.formatJson(rspData),
				response.isOk()? BussStatus.success: BussStatus.failed);
		} catch(Throwable e)
		{
			log.info("failed to fuiou", e);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				e.toString(), BussStatus.failed);

			pack(e, response);
		}
		
		return response;
	}

	@Override
	public QueryDetailResponse queryDetail(QueryDetailRequest request) 
	{
		QueryDetailResponse response = new QueryDetailResponse();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getCommTransFlow());

		QueryReqData reqData = new QueryReqData();
		
		reqData.setMchnt_cd(merchantid);
		reqData.setMchnt_txn_ssn(request.getTradeNo());

		reqData.setStart_day(shortDateFormater.format(request.getStartTime()));
		reqData.setEnd_day(shortDateFormater.format(request.getEndTime()));

		if (StringUtils.isNotBlank(request.getAccountId()))
			reqData.setUser_ids(request.getAccountId());

		response.setTradeNo(request.getTradeNo());
		response.setPage(request.getPage());
		response.setSize(request.getSize());

		try
		{
			payLoggerService.log(
				Utils.now(), request.getTradeNo(),
				BussType.custom, "query-detail",
				channel,
				request.getAccountId(), null,
				JsonHelper.formatJson(reqData), null,
				BussStatus.pending);
		
			QueryRspData rspData = FuiouService.query(reqData);

			log.info(String.format("rspData = %s", JsonHelper.formatJson(rspData)));
	
			response.setStatus(Integer.parseInt(rspData.getResp_code()));

			response.setTotalElements(0);
			if (CollectionUtils.isNotEmpty(rspData.getOpResultSet()))
			{
				QueryOpResultSet accountDetailResults =
					rspData.getOpResultSet().get(0);

				response.setTotal(Long.parseLong(
					accountDetailResults.getCt_balance()));
				response.setAvalidated(Long.parseLong(
					accountDetailResults.getCa_balance()));
				response.setUncarried(Long.parseLong(
					accountDetailResults.getCu_balance()));
				response.setFreezed(Long.parseLong(
					accountDetailResults.getCf_balance()));

				if (CollectionUtils
						.isNotEmpty(accountDetailResults.getDetails()))
				{
					ArrayList<DetailRecord> records =
						new ArrayList<DetailRecord>(
							accountDetailResults.getDetails().size());
					response.setResult(records);
					
					for(QueryDetail detail: accountDetailResults.getDetails())
					{
						DetailRecord record = new DetailRecord();

						record.setTimestamp(
							new Timestamp(shortDateFormater.parse(
								detail.getRec_crt_ts()).getTime()));

						record.setRem(detail.getBook_digest());
						
						record.setTotal(Long.parseLong(detail.getCt_balance()));
						record.setInTotal(
							Long.parseLong(detail.getCt_credit_amt()));
						record.setOutTotal(
							Long.parseLong(detail.getCt_debit_amt()));

						record.setAvalidated(Long.parseLong(detail.getCa_balance()));
						record.setInAvalidated(
							Long.parseLong(detail.getCa_credit_amt()));
						record.setOutAvalidated(
							Long.parseLong(detail.getCa_debit_amt()));
						
						record.setFreezed(Long.parseLong(detail.getCf_balance()));
						record.setInFreezed(
							Long.parseLong(detail.getCf_credit_amt()));
						record.setOutFreezed(
							Long.parseLong(detail.getCf_debit_amt()));
						
						record.setUncarried(Long.parseLong(detail.getCu_balance()));
						record.setInUncarried(
							Long.parseLong(detail.getCu_credit_amt()));
						record.setOutUncarried(
							Long.parseLong(detail.getCu_debit_amt()));
						
						records.add(record);
					}
				}
			}
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				JsonHelper.formatJson(rspData),
				response.isOk()? BussStatus.success: BussStatus.failed);
		} catch(Throwable e)
		{
			log.info("failed to fuiou", e);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				e.toString(), BussStatus.failed);

			pack(e, response);
		}
		
		return response;
	}

	@Override
	public BalanceResponse queryBalance(BalanceRequest request) 
	{
		BalanceResponse response = new BalanceResponse();

		if (request.getReason() == BalanceRequest.Reason.common &&
			request.getAccountId() == null)
			return response;

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getCommTransFlow());

		QueryBalanceReqData reqData = new QueryBalanceReqData();
		
		reqData.setMchnt_cd(merchantid);
		reqData.setMchnt_txn_ssn(request.getTradeNo());
		reqData.setMchnt_txn_dt(shortDateFormater.format(Utils.now()));
		reqData.setCust_no(request.getAccountId());

		if (request.getReason() == BalanceRequest.Reason.company)
		{
			reqData.setCust_no(companyAccountId);
		} else
		if (request.getReason() == BalanceRequest.Reason.receivable)
		{
			reqData.setCust_no(receivableAccountId);
		}

		response.setTradeNo(request.getTradeNo());

		try
		{
			payLoggerService.log(
				Utils.now(), request.getTradeNo(),
				BussType.custom, "query-balance",
				channel,
				request.getAccountId(), null,
				JsonHelper.formatJson(reqData), null,
				BussStatus.pending);

			QueryBalanceRspData rspData = FuiouService.balanceAction(reqData);

			response.setStatus(Integer.parseInt(rspData.getResp_code()));
			
			if (CollectionUtils.isNotEmpty(rspData.getResults()))
			{
				QueryBalanceResultData resultData = rspData.getResults().get(0);

				response.setAccountId(resultData.getUser_id());
				
				if (StringUtils.isNotBlank(resultData.getCt_balance()))
				{
					response.setTotal(
						Long.parseLong(resultData.getCt_balance()));
				}
				
				if (StringUtils.isNotBlank(resultData.getCt_balance()))
				{
					response.setAvalidated(
						Long.parseLong(resultData.getCa_balance()));
				}
				if (StringUtils.isNotBlank(resultData.getCt_balance()))
				{
					response.setUncarried(
						Long.parseLong(resultData.getCu_balance()));
				}
				if (StringUtils.isNotBlank(resultData.getCt_balance()))
				{
					response.setFreezed(
						Long.parseLong(resultData.getCf_balance()));
				}
			}
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				JsonHelper.formatJson(rspData),
				response.isOk()? BussStatus.success: BussStatus.failed);
		} catch(Throwable e)
		{
			log.info("failed to fuiou", e);
			
			payLoggerService.logback(
				Utils.now(), request.getTradeNo(),
				e.toString(), BussStatus.failed);

			pack(e, response);
		}

		return response;
	}

	@Override
	public RedirectResponse redirect(RedirectRequest request)
	{
		DepositResponse response = new DepositResponse();

		if (request.getTradeNo() == null)
			request.setTradeNo(getUniqueNoService.getPaymentTransFlow());

		response.setTradeNo(request.getTradeNo());

		if (request.getMethod() == RedirectRequest.Method.modifyCard)
		{
			if (request.getReason() == RedirectRequest.Reason.app)
			{
				response.setTargetUrl(
					String.format("%s/app/appChangeCard.action", fuyouUrl));
			} else
			{
				response.setTargetUrl(
					String.format("%s/changeCard2.action", fuyouUrl));
			}
		} else
		if (request.getMethod() == RedirectRequest.Method.modifyPhone)
		{
			response.setTargetUrl(
				String.format("%s/%s400101.action", fuyouUrl,
					(request.getReason() == RedirectRequest.Reason.app)?
						"app/": ""));
		} else
		if (request.getMethod() == RedirectRequest.Method.modifyPassword)
		{
			response.setTargetUrl(
				String.format("%s/%sresetPassWord.action", fuyouUrl,
					(request.getReason() == RedirectRequest.Reason.app)?
						"app/": ""));
		}

		if (request.getMethod() == RedirectRequest.Method.modifyCard ||
			request.getMethod() == RedirectRequest.Method.modifyPhone)
		{
			response.setBody(new MapBuilder<String, Object>()
				.append("mchnt_cd", merchantid)
				.append("mchnt_txn_ssn", request.getTradeNo())
				.append("login_id", request.getAccountId())
				.append("page_notify_url", request.getNotifyUrl())
				.append("signature",
					SecurityUtils.sign(Utils.joinStr(
						new String[] {
							request.getAccountId(),
							merchantid,
							request.getTradeNo(),
							request.getNotifyUrl()
						}, "|")))
				.toMap());
		} else
		if (request.getMethod() == RedirectRequest.Method.modifyPassword)
		{
			response.setBody(new MapBuilder<String, Object>()
				.append("mchnt_cd", merchantid)
				.append("mchnt_txn_ssn", request.getTradeNo())
				.append("login_id", request.getAccountId())
				.append("busi_tp", "3")
				.append("signature",
					SecurityUtils.sign(Utils.joinStr(
						new String[] {
							"3", request.getAccountId(),
							merchantid,
							request.getTradeNo()
						}, "|")))
				.toMap());
		}

		response.setTargetMethod("post");
		response.setTargetType("application/x-www-form-urlencoded");
		response.setTargetBody(HttpHelper.encodeForm(response.getBody()));

		payLoggerService.log(
			Utils.now(), request.getTradeNo(),
			BussType.custom, "redirect." + request.getMethod(),
			channel,
			request.getAccountId(), null,
			JsonHelper.formatJson(response), null,
			BussStatus.pending);
		
		return response;
	}
	
	public static <T extends PayResponse>
		CommonRspData pack(CommonRspData rspData, T response)
	{
		response.setStatus(Integer.parseInt(rspData.getResp_code()));

		return rspData;
	}

	public static <T extends PayResponse>
		T pack(Throwable exception, T response)
	{
		response.setStatus(-500);
		response.setMessage(exception.getMessage());

		return response;
	}

	@Override
	public String getMerchantId()
	{
		return merchantid;
	}

	@Override
	public void afterPropertiesSet()
		throws Exception
	{
		if (payLoggerService == null)
		{
			payLoggerService = new PayLoggerService() {

				@Override
				public void log(Timestamp timestamp, String tradeNo,
					BussType type, String busDesc, String channel,
					String custNo, String targetNo, String request,
					String response, BussStatus status)
				{ }

				@Override
				public void logback(Timestamp timestamp,
					String tradeNo, String response, BussStatus status)
				{ }

				@Override
				public String queryLogField(String tradeNo, String fieldName)
				{
					return "";
				}

				@Override
				public String queryLogJon(String tradeNo)
				{
					return "{ }";
				}
			};
		}
	}

	@Override
	public String getUrl(String path)
	{
		return String.format("%s%s", fuyouUrl, path);
	}

	@Override
	public String signature(String[] params)
	{
		return SecurityUtils.sign(Utils.joinStr(params, "|"));
	}
}
