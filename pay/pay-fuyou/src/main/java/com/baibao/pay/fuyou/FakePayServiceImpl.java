package com.baibao.pay.fuyou;

import com.baibao.pay.PayService;
import com.baibao.pay.data.AccountInfoRequest;
import com.baibao.pay.data.AccountInfoResponse;
import com.baibao.pay.data.ActiveAccountRequest;
import com.baibao.pay.data.ActiveAccountResponse;
import com.baibao.pay.data.BalanceRequest;
import com.baibao.pay.data.BalanceResponse;
import com.baibao.pay.data.DepositRequest;
import com.baibao.pay.data.DepositResponse;
import com.baibao.pay.data.FreezeRequest;
import com.baibao.pay.data.FreezeResponse;
import com.baibao.pay.data.HostingCollectRequest;
import com.baibao.pay.data.HostingCollectResponse;
import com.baibao.pay.data.HostingPayRequest;
import com.baibao.pay.data.HostingPayResponse;
import com.baibao.pay.data.HostingRefundRequest;
import com.baibao.pay.data.HostingRefundResponse;
import com.baibao.pay.data.ModifyAccountRequest;
import com.baibao.pay.data.ModifyAccountResponse;
import com.baibao.pay.data.QueryDepositRequest;
import com.baibao.pay.data.QueryDepositResponse;
import com.baibao.pay.data.QueryDetailRequest;
import com.baibao.pay.data.QueryDetailResponse;
import com.baibao.pay.data.QueryHostingTradeRequest;
import com.baibao.pay.data.QueryHostingTradeResponse;
import com.baibao.pay.data.QueryWithdrawRequest;
import com.baibao.pay.data.QueryWithdrawResponse;
import com.baibao.pay.data.RedirectRequest;
import com.baibao.pay.data.RedirectResponse;
import com.baibao.pay.data.UnfreezeRequest;
import com.baibao.pay.data.UnfreezeResponse;
import com.baibao.pay.data.WithdrawRequest;
import com.baibao.pay.data.WithdrawResponse;

public class FakePayServiceImpl
	implements PayService
{

	@Override
	public String getMerchantId()
	{
		return "";
	}

	@Override
	public ActiveAccountResponse activeAccount(ActiveAccountRequest request)
	{
		ActiveAccountResponse response = new ActiveAccountResponse();

		response.setAccountId(request.getPhone());
		
		return response;
	}

	@Override
	public ModifyAccountResponse modifyAccount(ModifyAccountRequest request)
	{
		ModifyAccountResponse response = new ModifyAccountResponse();

		return response;
	}

	@Override
	public AccountInfoResponse getAccountInfo(AccountInfoRequest request) 
	{
		AccountInfoResponse response = new AccountInfoResponse();

		return response;
	}

	@Override
	public FreezeResponse freeze(FreezeRequest request) 
	{
		FreezeResponse response = new FreezeResponse();

		return response;
	}

	@Override
	public UnfreezeResponse unfreeze(UnfreezeRequest request) 
	{
		UnfreezeResponse response = new UnfreezeResponse();

		return response;
	}

	@Override
	public HostingCollectResponse hostingCollect(HostingCollectRequest request) 
	{
		HostingCollectResponse response = new HostingCollectResponse();

		return response;
	}

	@Override
	public HostingPayResponse hostingPay(HostingPayRequest request) 
	{
		HostingPayResponse response = new HostingPayResponse();

		return response;
	}

	@Override
	public HostingRefundResponse hostingRefund(HostingRefundRequest request) 
	{
		HostingRefundResponse response = new HostingRefundResponse();

		return response;
	}

	@Override
	public DepositResponse deposit(DepositRequest request) 
	{
		DepositResponse response = new DepositResponse();

		return response;
	}

	@Override
	public DepositResponse recharge(DepositRequest request) 
	{
		DepositResponse response = new DepositResponse();

		return response;
	}

	@Override
	public WithdrawResponse withdraw(WithdrawRequest request) 
	{
		WithdrawResponse response = new WithdrawResponse();

		return response;
	}

	@Override
	public QueryDepositResponse queryDeposit(QueryDepositRequest request) 
	{
		QueryDepositResponse response = new QueryDepositResponse();

		return response;
	}

	@Override
	public QueryWithdrawResponse queryWithdraw(QueryWithdrawRequest request) 
	{
		QueryWithdrawResponse response = new QueryWithdrawResponse();

		return response;
	}

	@Override
	public QueryHostingTradeResponse queryHostingTrade(QueryHostingTradeRequest request) 
	{
		QueryHostingTradeResponse response = new QueryHostingTradeResponse();

		return response;
	}

	@Override
	public QueryDetailResponse queryDetail(QueryDetailRequest request) 
	{
		QueryDetailResponse response = new QueryDetailResponse();

		return response;
	}

	@Override
	public BalanceResponse queryBalance(BalanceRequest request) 
	{
		BalanceResponse response = new BalanceResponse();

		response.setTotal(1000000l);
		response.setAvalidated(1000000l);
		
		return response;
	}

	@Override
	public RedirectResponse redirect(RedirectRequest request)
	{
		RedirectResponse response = new RedirectResponse();

		return response;
	}

	@Override
	public String getUrl(String path) {
		return null;
	}

	@Override
	public String signature(String[] params) {
		return null;
	}

}
