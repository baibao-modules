package com.baibao.pay.fuyou;

import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.baibao.pay.GetUniqueNoService;
import com.baibao.utils.Utils;

/**
 * 
 * @author Denny
 * @date 2016年6月21日
 * 
 */
public class GetUniqueNoServiceImpl
	implements GetUniqueNoService
{
    protected Random random;
    protected SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");

    public GetUniqueNoServiceImpl()
    {
        long seed = this.hashCode() + System.currentTimeMillis();

        random = new Random(seed);
    }

    protected String makePrefixNo(String prefix)
    {
        return String.format("%s%s%06d",
        	prefix, formater.format(Utils.now()),
        	(long) (random.nextDouble() * 999999));
    }
    
    protected String makePrefixNo(int prefix)
    {
        return String.format("%05d%s%06d",
        	prefix, formater.format(Utils.now()),
        	(long) (random.nextDouble() * 999999));
    }

    @Override
    public String getCustNo()
    {
        UUID uuid = UUID.randomUUID();

        return uuid.toString();
    }

    @Override
    public String getPaymentTransFlow()
    {
        return makePrefixNo(2);
    }

    @Override
    public String getWithDrawTransFlow()
    {
        return makePrefixNo(3);
    }

    @Override
    public String getCardTransFlow()
    {
        return makePrefixNo(4);
    }

    @Override
    public String getProjectNo()
    {
        return makePrefixNo("BBJF0");
    }

    @Override
    public String getCommTransFlow()
    {
        return makePrefixNo(6);
    }

    @Override
    public String getOrderNo()
    {
        return makePrefixNo(7);
    }

    @Override
    public String getBidRTransFlow()
    {
        return makePrefixNo(8);
    }

    @Override
    public String getBidPTransFlow()
    {
        return makePrefixNo(9);
    }

    @Override
    public String getRepaymentTransFlow()
    {
        return makePrefixNo(10);
    }

    @Override
    public String getRepaymentTransFlow2()
    {
        return makePrefixNo(11);
    }

    @Override
    public String getCreditorTransFlow()
    {
        return makePrefixNo(12);
    }

    @Override
    public String getCreditorTransFlow2()
    {
        return makePrefixNo(13);
    }

    @Override
    public String getPutongTransFlow()
    {
        return makePrefixNo(14);
    }

    @Override
    public String getPutongTransFlow2()
    {
        return makePrefixNo(15);
    }

    @Override
    public String getIncomeFlow()
    {
        return makePrefixNo(16);
    }

    @Override
    public String getRepayBillFlow()
    {
        return makePrefixNo(17);
    }

    @Override
    public String getActivityTransFlow()
    {
        return makePrefixNo(18);
    }

    @Override
    public String getActivityTransFlow2()
    {
        return makePrefixNo(19);
    }

    @Override
    public String getRepayTransFlow()
    {
        return makePrefixNo(20);
    }

    @Override
    public String getRepayTransFlow2()
    {
        return makePrefixNo(21);
    }

    @Override
    public String getActiveAccountFlow()
    {
        return makePrefixNo(22);
    }

    @Override
    public String getQueryAccountFlow()
    {
        return makePrefixNo(23);
    }

    @Override
    public String getModifyAccountFlow()
    {
        return makePrefixNo(24);
    }

    @Override
    public String getFreezeFlow()
    {
        return makePrefixNo(25);
    }

    @Override
    public String getUnfreezeFlow()
    {
        return makePrefixNo(26);
    }
}
