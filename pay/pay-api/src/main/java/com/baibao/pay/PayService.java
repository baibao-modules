package com.baibao.pay;

import com.baibao.pay.data.AccountInfoRequest;
import com.baibao.pay.data.AccountInfoResponse;
import com.baibao.pay.data.ActiveAccountRequest;
import com.baibao.pay.data.ActiveAccountResponse;
import com.baibao.pay.data.BalanceRequest;
import com.baibao.pay.data.BalanceResponse;
import com.baibao.pay.data.DepositRequest;
import com.baibao.pay.data.DepositResponse;
import com.baibao.pay.data.FreezeRequest;
import com.baibao.pay.data.FreezeResponse;
import com.baibao.pay.data.HostingCollectRequest;
import com.baibao.pay.data.HostingCollectResponse;
import com.baibao.pay.data.HostingPayRequest;
import com.baibao.pay.data.HostingPayResponse;
import com.baibao.pay.data.HostingRefundRequest;
import com.baibao.pay.data.HostingRefundResponse;
import com.baibao.pay.data.ModifyAccountRequest;
import com.baibao.pay.data.ModifyAccountResponse;
import com.baibao.pay.data.QueryDepositRequest;
import com.baibao.pay.data.QueryDepositResponse;
import com.baibao.pay.data.QueryDetailRequest;
import com.baibao.pay.data.QueryDetailResponse;
import com.baibao.pay.data.QueryHostingTradeRequest;
import com.baibao.pay.data.QueryHostingTradeResponse;
import com.baibao.pay.data.QueryWithdrawRequest;
import com.baibao.pay.data.QueryWithdrawResponse;
import com.baibao.pay.data.RedirectRequest;
import com.baibao.pay.data.RedirectResponse;
import com.baibao.pay.data.UnfreezeRequest;
import com.baibao.pay.data.UnfreezeResponse;
import com.baibao.pay.data.WithdrawRequest;
import com.baibao.pay.data.WithdrawResponse;

public interface PayService
{
	public String getMerchantId();
	public String getUrl(String path);
	public String signature(String[] params);

	/**
	 * 激活/开户
	 */
	public ActiveAccountResponse activeAccount(ActiveAccountRequest request);
	
	/**
	 * 修改帐户信息
	 */
	public ModifyAccountResponse modifyAccount(ModifyAccountRequest request);

	/**
	 * 获取帐户信息
	 */
	public AccountInfoResponse getAccountInfo(AccountInfoRequest request);

	/*
	public Map<String, Object> activeMember(Map<String,String> parameters); //激活会员账户

	public Map<String, Object> set_real_name(Map<String,String> parameters);//设置实名认证

	public Map<String, Object> binding_verify(Map<String,String> parameters);//绑定认证信息

	public Map<String, Object> binding_bank_card(Map<String,String> parameters);//绑定银行卡

	public Map<String, Object> binding_bank_card_advance(Map<String,String> parameters);//绑定银行卡推进

	public Map<String, Object> unbinding_bank_card(Map<String,String> parameters);//解绑银行卡

	public Map<String, Object> unbinding_bank_card_advance(Map<String,String> parameters);//解绑解行卡推进

	public Map<String, Object> query_bank_card(Map<String,String> parameters);//查询银行卡
    */

	/**
	 * 冻结余额
	 */
	public FreezeResponse freeze(FreezeRequest request);

	/**
	 * 解冻余额
	 */
	public UnfreezeResponse unfreeze(UnfreezeRequest request);

	/*
	public Map<String, Object> balance_freeze(Map<String,String> parameters);//冻结余额

	public Map<String, Object> balance_unfreeze(Map<String,String> parameters);//解冻余额
	*/

	/**
	 * 托管代收
	 */
	public HostingCollectResponse hostingCollect(HostingCollectRequest request);
	
	/**
	 * 托管代付
	 */
	public HostingPayResponse hostingPay(HostingPayRequest request);

	/*
	public Map<String, Object> create_hosting_collect_trade(Map<String,String> parameters);//创建托管代收交易
	public Map<String, Object> create_single_hosting_pay_trade(Map<String,String> parameters);//创建托管代付交易
	public Map<String, Object> create_batch_hosting_pay_trade(Map<String,String> parameters);//创建批量托管代付交易
	*/

	/**
	 * 托管退款
	 */
	public HostingRefundResponse hostingRefund(HostingRefundRequest request);

	/*
	public Map<String, Object> create_hosting_refund(Map<String,String> parameters); //托管退款
	*/

	/**
	 * 托管充值
	 */
	public DepositResponse deposit(DepositRequest request);
	
	/**
	 * 网银充值
	 */
	public DepositResponse recharge(DepositRequest request);
	
	/**
	 * 托管提现
	 */
	public WithdrawResponse withdraw(WithdrawRequest request);

	/*
	public Map<String, Object> create_hosting_deposit(Map<String,String> parameters); //托管充值
	public Map<String, Object> online_banking_recharge(Map<String,String> parameters);//网银充值
	public Map<String, Object> create_hosting_withdraw(Map<String,String> parameters);//托管提现
	 */

	/**
	 * 托管充值查询
	 */
	public QueryDepositResponse queryDeposit(QueryDepositRequest request);
	
	/**
	 * 托管提现查询
	 */
	public QueryWithdrawResponse queryWithdraw(QueryWithdrawRequest request);
	
	/**
	 * 收支明细
	 */
	public QueryHostingTradeResponse
		queryHostingTrade(QueryHostingTradeRequest request);
	
	/**
	 * 交易查询
	 */
	public QueryDetailResponse queryDetail(QueryDetailRequest request);
	
	/*
	public Map<String, Object> query_hosting_deposit(Map<String,String> parameters);  //托管充值查询
	public Map<String, Object> query_hosting_withdraw(Map<String,String> parameters); //托管提现查询
	public Map<String, Object> query_account_details(Map<String,String> parameters ); //查询收支明细
	public Map<String, Object> query_hosting_trade(Map<String,String> parameters);    //托管交易查询
	 */
	
	/*
	public Map<String, Object> query_hosting_refund(Map<String,String> parameters); //托管退款交易查询
	public Map<String, Object> query_hosting_batch_trade(Map<String,String> parameters);//托管交易批次查询
	public Map<String, Object> advance_hosting_pay(Map<String,String> parameters);//支付推进
	public Map<String, Object> unbinding_verify(Map<String,String> parameters);//解绑认证信息
	*/

	/**
	 * 查询余额
	 */
	public BalanceResponse queryBalance(BalanceRequest request);

	/*
	public Map<String, Object> query_balance(Map<String,String> parameters);//查询余额
	public Map<String, Object> query_fundyield(Map<String,String> parameters);
	public Map<String, Object> query_verify(Map<String,String> parameters);
	*/
	
	/**
	 * 其他跳转页面请求
	 */
	public RedirectResponse redirect(RedirectRequest request);
}
