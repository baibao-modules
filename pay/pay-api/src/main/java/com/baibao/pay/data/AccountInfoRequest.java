package com.baibao.pay.data;

import com.sun.istack.NotNull;

public class AccountInfoRequest
	extends PayRequest
{
	@NotNull
	private String accountId;
	
	private String rem = "";

	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
}
