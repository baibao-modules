package com.baibao.pay.data;

import com.sun.istack.NotNull;

public class ModifyAccountRequest
	extends PayRequest
{
	@NotNull
	private String accountId;

	@NotNull
	private String customName;
	
	@NotNull
	private String certId;
	
	@NotNull
	private String phone;
	
	private String email;
	
	private BankCard bankCard;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCustomName() {
		return customName;
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}

	public String getCertId() {
		return certId;
	}

	public void setCertId(String certId) {
		this.certId = certId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BankCard getBankCard() {
		return bankCard;
	}

	public void setBankCard(BankCard bankCard) {
		this.bankCard = bankCard;
	}
}
