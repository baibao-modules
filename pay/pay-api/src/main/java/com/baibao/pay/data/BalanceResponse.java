package com.baibao.pay.data;

public class BalanceResponse
	extends PayResponse
{
	private String accountId;

	/** 总额 */
	private Long total = 0l;
	
	/** 有效余额 */
	private Long avalidated = 0l;

	/** 冻结余额 */
	private Long freezed = 0l;

	/** 未转结余额 */
	private Long uncarried = 0l;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getAvalidated() {
		return avalidated;
	}

	public void setAvalidated(Long avalidated) {
		this.avalidated = avalidated;
	}

	public Long getFreezed() {
		return freezed;
	}

	public void setFreezed(Long freezed) {
		this.freezed = freezed;
	}

	public Long getUncarried() {
		return uncarried;
	}

	public void setUncarried(Long uncarried) {
		this.uncarried = uncarried;
	}
}
