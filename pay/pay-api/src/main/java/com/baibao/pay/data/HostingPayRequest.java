package com.baibao.pay.data;

public class HostingPayRequest
	extends PayRequest
{
	public static enum Reason
	{
		common, //普通付款
		lending, //放贷
		income, //收益
	}

	private String payeeAccountId;
	
	private Long amount;

	private Reason reason = Reason.common;

	public String getPayeeAccountId() {
		return payeeAccountId;
	}

	public void setPayeeAccountId(String payeeAccountId) {
		this.payeeAccountId = payeeAccountId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getRem() {
		return String.valueOf(reason);
	}

	public Reason getReason() {
		return reason;
	}

	public void setReason(Reason reason) {
		this.reason = reason;
	}
}
