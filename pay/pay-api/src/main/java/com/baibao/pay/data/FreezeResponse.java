package com.baibao.pay.data;

public class FreezeResponse
	extends PayResponse
{
	private String targetUrl;
	private String targetMethod;
	private String targetType;
	private String targetBody;
	
	public String getTargetUrl() {
		return targetUrl;
	}
	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}
	public String getTargetMethod() {
		return targetMethod;
	}
	public void setTargetMethod(String targetMethod) {
		this.targetMethod = targetMethod;
	}
	public String getTargetType() {
		return targetType;
	}
	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}
	public String getTargetBody() {
		return targetBody;
	}
	public void setTargetBody(String targetBody) {
		this.targetBody = targetBody;
	}
}
