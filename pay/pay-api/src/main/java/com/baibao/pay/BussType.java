package com.baibao.pay;

public enum BussType
{
	bind(8, "bind"),
	recharge(10, "recharge"),
	withdraw(11, "withdraw"),
	tender(12, "tender"),
	collecting(13, "collecting"),
	paying(14, "paying"),
	refund(40, "refund"),
	custom(-1, "custom");

	BussType(Integer code, String desc)
	{
		this.code = code;
		this.desc = desc;
	}

	private Integer code;
	private String desc;

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
}
