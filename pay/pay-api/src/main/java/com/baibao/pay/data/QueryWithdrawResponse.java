package com.baibao.pay.data;

import java.sql.Timestamp;
import java.util.List;

public class QueryWithdrawResponse
	extends PayResponse
{
	public static class WithdrawRecord
	{
		private Timestamp datetime;
		
		private String tradeNo;
		
		private String accountId;
		private String accountName;
		
		private Long amount;

		private String rem = "";

		private Boolean successed;
		private String message;

		public Timestamp getDatetime() {
			return datetime;
		}
		public void setDatetime(Timestamp datetime) {
			this.datetime = datetime;
		}
		public String getTradeNo() {
			return tradeNo;
		}
		public void setTradeNo(String tradeNo) {
			this.tradeNo = tradeNo;
		}
		public String getAccountId() {
			return accountId;
		}
		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}
		public String getAccountName() {
			return accountName;
		}
		public void setAccountName(String accountName) {
			this.accountName = accountName;
		}
		public Long getAmount() {
			return amount;
		}
		public void setAmount(Long amount) {
			this.amount = amount;
		}
		public String getRem() {
			return rem;
		}
		public void setRem(String rem) {
			this.rem = rem;
		}
		public Boolean getSuccessed() {
			return successed;
		}
		public void setSuccessed(Boolean successed) {
			this.successed = successed;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
	}
	
	private int totalElements;
	private int page;
	private int size;

	private List<WithdrawRecord> result;

	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}

	public List<WithdrawRecord> getResult() {
		return result;
	}

	public void setResult(List<WithdrawRecord> result) {
		this.result = result;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
}
