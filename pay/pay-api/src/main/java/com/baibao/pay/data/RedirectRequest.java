package com.baibao.pay.data;

import com.baibao.pay.data.DepositRequest.Reason;

public class RedirectRequest
	extends PayRequest
{
	public static enum Reason
	{
		web,
		app
	}

	public static enum Method
	{
		modifyPhone,
		modifyCard,
		modifyPassword
	}

	private String accountId;

	private String rem = "";

	private String notifyUrl;

	private Reason reason = Reason.web;
	private Method method = Method.modifyPhone;

	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getRem() {
		return rem;
	}
	public void setRem(String rem) {
		this.rem = rem;
	}
	public String getNotifyUrl() {
		return notifyUrl;
	}
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
	public Reason getReason() {
		return reason;
	}
	public void setReason(Reason reason) {
		this.reason = reason;
	}
	public Method getMethod() {
		return method;
	}
	public void setMethod(Method method) {
		this.method = method;
	}

	
}
