package com.baibao.pay.data;

public class PayRequest
{
	private String tradeNo;

	public String getTradeNo()
	{
		return tradeNo;
	}

	public void setTradeNo(String tradeNo)
	{
		this.tradeNo = tradeNo;
	}
}
