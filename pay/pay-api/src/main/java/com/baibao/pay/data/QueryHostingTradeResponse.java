package com.baibao.pay.data;

import java.sql.Timestamp;
import java.util.List;

public class QueryHostingTradeResponse
	extends PayResponse
{
	public static class HostingTradeRecord
	{
		private Timestamp timestamp;
		
		private String tradeNo;
		
		private Long amount;
		
		private String accountId;
		private String accountName;
		
		private String inAccountId;
		private String inAccountName;

		private Boolean successed;
		private String message;
		public Timestamp getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(Timestamp timestamp) {
			this.timestamp = timestamp;
		}
		public String getTradeNo() {
			return tradeNo;
		}
		public void setTradeNo(String tradeNo) {
			this.tradeNo = tradeNo;
		}
		public Long getAmount() {
			return amount;
		}
		public void setAmount(Long amount) {
			this.amount = amount;
		}
		public String getAccountId() {
			return accountId;
		}
		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}
		public String getAccountName() {
			return accountName;
		}
		public void setAccountName(String accountName) {
			this.accountName = accountName;
		}
		public Boolean getSuccessed() {
			return successed;
		}
		public void setSuccessed(Boolean successed) {
			this.successed = successed;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public String getInAccountId() {
			return inAccountId;
		}
		public void setInAccountId(String inAccountId) {
			this.inAccountId = inAccountId;
		}
		public String getInAccountName() {
			return inAccountName;
		}
		public void setInAccountName(String inAccountName) {
			this.inAccountName = inAccountName;
		}
	}

	private int totalElements;
	private int page;
	private int size;

	private List<HostingTradeRecord> result;

	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}

	public List<HostingTradeRecord> getResult() {
		return result;
	}

	public void setResult(List<HostingTradeRecord> record) {
		this.result = result;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
