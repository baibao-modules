package com.baibao.pay;

public interface GetUniqueNoService
{
	/**
	 * 获取客户号(length:23,prefix:100)
	 * @return
	 */
	public String getCustNo();
	
	/**
	 * 获取充值流水号(length:30,prefix:101)
	 * @return
	 */
	public String getPaymentTransFlow() ;

	/**
	 * 获取提现流水号(length:30,prefix:102)
	 * 
	 * @return
	 */
	public String getWithDrawTransFlow() ;
	
	/**
	 * 绑卡流水号(length:30,prefix:103)
	 * 
	 * @return
	 */
	public String getCardTransFlow() ;
	
	/**
	 * 获取产品编号(length:24,prefix:QZB)
	 * @return
	 */
	public String getProjectNo() ;
	
	/**
	 * 通用流水号
	 * @return
	 */
	public String getCommTransFlow() ;
	
	public String getOrderNo() ;

	/**
	 * 获取投标流水：对应代收 length:30,prefix:I
	 * 获取投标流水(对应代收)length:32,prefix:103
	 * @return
	 */
	public String getBidRTransFlow() ;

	/**
	 * 获取投标流水：对应代付 length:30,prefix:O
	 * 获取投标流水(对应代付)length:32,prefix:104
	 * @return
	 */
	public String getBidPTransFlow();
	/**
	 * 获取还款流水：对应借款人代收 length:29,prefix:I
	 * 获取还款流水(对应借款人代收)length:32,prefix:105
	 * @return
	 */
	public String getRepaymentTransFlow();
	/**
	 * 获取还款流水：对应借款人代付 length:29,prefix:O
	 * 获取还款流水(对应借款人代付)length:32,prefix:106
	 * @return
	 */
	public String getRepaymentTransFlow2();

	/**
	 * 获取购买债权流水：对应代收 length:28,prefix:I
	 * 获取购买债权流水(对应代收)length:32,prefix:107
	 * @return
	 */
	public String getCreditorTransFlow();

	/**
	 * 获取购买债权流水：对应代收 length:28,prefix:O
	 * 获取购买债权流水(对应代收)length:32,prefix:108
	 * @return
	 */
	public String getCreditorTransFlow2();
	/**
	 * 获取普通代收流水：对应代收 length:30,prefix:201
	 * @return
	 */
	public String getPutongTransFlow();

	/**
	 * 获取普通代收流水：对应代付 length:30,prefix:202
	 * @return
	 */
	public String getPutongTransFlow2();
	
	/**
	 * 收益流水
	 * @return
	 */
	public String getIncomeFlow();
	/**
	 * 应还账单
	 * @return
	 */
	public String getRepayBillFlow();
	
	/**
	 * 获取活动转账代收流水：对应代收 length:30,prefix:201
	 * @return
	 */
	public String getActivityTransFlow();

	/**
	 * 获取活动代付流水：对应代付 length:30,prefix:202
	 * @return
	 */
	public String getActivityTransFlow2();
	
	/**
	 * 获取还款流水：对应借款人代收 length:29,prefix:I
	 * 获取还款流水(对应借款人代收)length:32,prefix:105
	 * @return
	 */
	public String getRepayTransFlow();

	/**
	 * 获取还款流水：对应借款人代付 length:29,prefix:O
	 * 获取还款流水(对应借款人代付)length:32,prefix:106
	 * @return
	 */
	public String getRepayTransFlow2();
	
	public String getActiveAccountFlow();
	
	public String getQueryAccountFlow();
	
	public String getModifyAccountFlow();
	
	public String getFreezeFlow();
	public String getUnfreezeFlow();
}
