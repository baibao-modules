package com.baibao.pay.data;

public class DepositRequest
	extends PayRequest
{
	public static enum Reason
	{
		web,
		app
	}

	private String accountId;

	private Long amount;

	private String rem = "";

	private String notifyUrl;
	
	private Reason reason = Reason.web;

	public Long getAmount() {
		return amount;
	}
	
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	
	public String getRem() {
		return rem;
	}
	
	public void setRem(String rem) {
		this.rem = rem;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public Reason getReason() {
		return reason;
	}

	public void setReason(Reason reason) {
		this.reason = reason;
	}
}
