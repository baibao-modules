package com.baibao.pay.data;

public class HostingCollectRequest
	extends PayRequest
{
	public static enum Reason
	{
		common, //普通收款
		repay, //还款
		invest, //投资
		interFee, //内部手续费等转账, 接收帐号到公司帐号
	}

	private String payAccountId;
	
	private Long amount;

	private Reason reason = Reason.common;

	public String getPayAccountId() {
		return payAccountId;
	}

	public void setPayAccountId(String payAccountId) {
		this.payAccountId = payAccountId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getRem() {
		return String.valueOf(reason);
	}

	public Reason getReason() {
		return reason;
	}

	public void setReason(Reason reason) {
		this.reason = reason;
	}
}
