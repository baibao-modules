package com.baibao.pay;

import java.sql.Timestamp;

public interface PayLoggerService
{
	public String queryLogField(String tradeNo, String fieldName);
	public String queryLogJon(String tradeNo);

	public void log(
		Timestamp timestamp, String tradeNo,
		BussType type, String busDesc, String channel,
		String custNo, String targetNo,
		String request, String response, BussStatus status);

	public void logback(
		Timestamp timestamp, String tradeNo,
		String response, BussStatus status);
}
