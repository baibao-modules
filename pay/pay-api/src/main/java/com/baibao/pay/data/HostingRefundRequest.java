package com.baibao.pay.data;

public class HostingRefundRequest
	extends PayRequest
{
	public static enum Reason
	{
		common, //普通退款
		cancelMark, //流标
	}

	private String accountId;

	private String orgiTradeNo;
	
	private Long amount;

	private Reason reason = Reason.common;
	
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Long getAmount() {
		return amount;
	}
	
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	
	public String getRem() {
		return String.valueOf(reason);
	}

	public String getOrgiTradeNo() {
		return orgiTradeNo;
	}

	public void setOrgiTradeNo(String orgiTradeNo) {
		this.orgiTradeNo = orgiTradeNo;
	}

	public Reason getReason() {
		return reason;
	}

	public void setReason(Reason reason) {
		this.reason = reason;
	}
}