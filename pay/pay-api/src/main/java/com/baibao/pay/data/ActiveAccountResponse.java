package com.baibao.pay.data;

public class ActiveAccountResponse
	extends PayResponse
{
	private String accountId;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
}
