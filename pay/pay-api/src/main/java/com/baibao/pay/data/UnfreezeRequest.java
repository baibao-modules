package com.baibao.pay.data;

public class UnfreezeRequest
	extends PayRequest
{
	private String accountId;
	
	private Long amount;
	
	private String rem = "";

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}
}
