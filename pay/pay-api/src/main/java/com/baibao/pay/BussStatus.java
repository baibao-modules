package com.baibao.pay;

public enum BussStatus
{
	success("00", "成功"),
	failed("01", "失败"),
	pending("02", "处理中");

	BussStatus(String code, String message)
	{
		this.code = code;
		this.message = message;
	}
	
	private String code;
	private String message;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
