package com.baibao.pay.data;

import com.sun.istack.NotNull;

public class BankCard
{
	@NotNull
	private String cityId;

	@NotNull
	private String bankId;
	
	private String bankName;
	
	@NotNull
	private String cardNumber;

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
}
