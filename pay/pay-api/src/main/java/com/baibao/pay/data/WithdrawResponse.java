package com.baibao.pay.data;

import java.util.Map;

public class WithdrawResponse
	extends PayResponse
{
	private String targetUrl;
	private String targetMethod;
	private String targetType;
	private String targetBody;

	private Map<String, Object> body;

	public String getTargetUrl() {
		return targetUrl;
	}
	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}
	public String getTargetMethod() {
		return targetMethod;
	}
	public void setTargetMethod(String targetMethod) {
		this.targetMethod = targetMethod;
	}
	public String getTargetType() {
		return targetType;
	}
	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}
	public String getTargetBody() {
		return targetBody;
	}
	public void setTargetBody(String targetBody) {
		this.targetBody = targetBody;
	}
	public Map<String, Object> getBody() {
		return body;
	}
	public void setBody(Map<String, Object> body) {
		this.body = body;
	}
}
