package com.baibao.pay.data;

import java.sql.Timestamp;
import java.util.List;

public class QueryDetailResponse
	extends PayResponse
{
	public static class DetailRecord
	{
		private String tradeNo;
		private Timestamp timestamp;
		
		/** 总额 */
		private Long total;
		private Long inTotal;
		private Long outTotal;
		
		/** 有效余额 */
		private Long avalidated;
		private Long inAvalidated;
		private Long outAvalidated;

		/** 冻结余额 */
		private Long freezed;
		private Long inFreezed;
		private Long outFreezed;

		/** 未转结余额 */
		private Long uncarried;
		private Long inUncarried;
		private Long outUncarried;

		private String rem = "";
		
		public String getTradeNo() {
			return tradeNo;
		}
		public void setTradeNo(String tradeNo) {
			this.tradeNo = tradeNo;
		}
		public Timestamp getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(Timestamp timestamp) {
			this.timestamp = timestamp;
		}
		public Long getTotal() {
			return total;
		}
		public void setTotal(Long total) {
			this.total = total;
		}
		public Long getInTotal() {
			return inTotal;
		}
		public void setInTotal(Long inTotal) {
			this.inTotal = inTotal;
		}
		public Long getOutTotal() {
			return outTotal;
		}
		public void setOutTotal(Long outTotal) {
			this.outTotal = outTotal;
		}
		public Long getAvalidated() {
			return avalidated;
		}
		public void setAvalidated(Long avalidated) {
			this.avalidated = avalidated;
		}
		public Long getInAvalidated() {
			return inAvalidated;
		}
		public void setInAvalidated(Long inAvalidated) {
			this.inAvalidated = inAvalidated;
		}
		public Long getOutAvalidated() {
			return outAvalidated;
		}
		public void setOutAvalidated(Long outAvalidated) {
			this.outAvalidated = outAvalidated;
		}
		public Long getFreezed() {
			return freezed;
		}
		public void setFreezed(Long freezed) {
			this.freezed = freezed;
		}
		public Long getInFreezed() {
			return inFreezed;
		}
		public void setInFreezed(Long inFreezed) {
			this.inFreezed = inFreezed;
		}
		public Long getOutFreezed() {
			return outFreezed;
		}
		public void setOutFreezed(Long outFreezed) {
			this.outFreezed = outFreezed;
		}
		public Long getUncarried() {
			return uncarried;
		}
		public void setUncarried(Long uncarried) {
			this.uncarried = uncarried;
		}
		public Long getInUncarried() {
			return inUncarried;
		}
		public void setInUncarried(Long inUncarried) {
			this.inUncarried = inUncarried;
		}
		public Long getOutUncarried() {
			return outUncarried;
		}
		public void setOutUncarried(Long outUncarried) {
			this.outUncarried = outUncarried;
		}
		public String getRem() {
			return rem;
		}
		public void setRem(String rem) {
			this.rem = rem;
		}
	}

	private int totalElements;
	private int page;
	private int size;
	
	private List<DetailRecord> result;
	
	/** 期初总额 */
	private Long total;
	
	/** 期初有效余额 */
	private Long avalidated;

	/** 期初冻结余额 */
	private Long freezed;

	/** 期初未转结余额 */
	private Long uncarried;

	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}

	public List<DetailRecord> getResult() {
		return result;
	}

	public void setResult(List<DetailRecord> result) {
		this.result = result;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getAvalidated() {
		return avalidated;
	}

	public void setAvalidated(Long avalidated) {
		this.avalidated = avalidated;
	}

	public Long getFreezed() {
		return freezed;
	}

	public void setFreezed(Long freezed) {
		this.freezed = freezed;
	}

	public Long getUncarried() {
		return uncarried;
	}

	public void setUncarried(Long uncarried) {
		this.uncarried = uncarried;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}
