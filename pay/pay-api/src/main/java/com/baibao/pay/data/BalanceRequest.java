package com.baibao.pay.data;

public class BalanceRequest
	extends PayRequest
{
	public static enum Reason
	{
		common, //普通查询
		company, //公司帐号查询
		receivable, //代收帐号查询
	}

	private String accountId;
	private Reason reason = Reason.common;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Reason getReason() {
		return reason;
	}

	public void setReason(Reason reason) {
		this.reason = reason;
	}
}
