package com.baibao.pay.data;

public class PayResponse
{
	public static final int OK = 0;

	//交易流水号
	private String tradeNo;

	private Integer status = OK;
	private String message;

	public boolean isOk()
	{
		return status == OK;
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
}
