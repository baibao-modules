package com.baibao.pay;

import java.io.File;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alibaba.druid.util.StringUtils;
import com.baibao.pay.data.AccountInfoRequest;
import com.baibao.pay.data.AccountInfoResponse;
import com.baibao.pay.data.BalanceRequest;
import com.baibao.pay.data.BalanceResponse;
import com.baibao.pay.data.HostingCollectRequest;
import com.baibao.pay.data.HostingCollectResponse;
import com.baibao.pay.data.HostingPayRequest;
import com.baibao.pay.data.HostingPayResponse;
import com.baibao.pay.data.QueryDetailRequest;
import com.baibao.pay.data.QueryDetailResponse;
import com.baibao.utils.JsonHelper;

import jodd.util.StringUtil;

public class PayMain
{
	public static class Options
	{
		@Option(name="-method", usage="method: balance", required = true)
		public String method;

		@Option(name="-account")
		public String account;

		@Option(name="-amount")
		public Long amount;
		
		@Option(name="-reason")
		public String reason;
		
		@Option(name="-start")
		public String start;
		
		@Option(name="-end")
		public String end;
	}

	private ApplicationContext context;
	private PayService payService;

	private PrintStream out;

	public PayMain()
		throws Exception
	{
		out = System.out;

		System.setOut(new PrintStream(new File("/dev/null")));

		context = new ClassPathXmlApplicationContext(
			"classpath*:/application-context.xml");
		
		payService = context.getBean("payService", PayService.class);
	}

	public void detail(Options option, String[] args)
		throws Exception
	{
		SimpleDateFormat format =
			new SimpleDateFormat("yyyy-MM-dd");
		
		QueryDetailRequest request = new QueryDetailRequest();
		
		request.setAccountId(option.account);
		request.setStartTime(new Timestamp(
			format.parse(option.start).getTime()));
		request.setEndTime(new Timestamp(
				format.parse(option.end).getTime()));
		
		QueryDetailResponse response = payService.queryDetail(request);

		out.println(JsonHelper.formatJson(response));
	}

	public void userinfo(Options option, String[] args)
	{
		AccountInfoRequest request = new AccountInfoRequest();

		request.setAccountId(option.account);

		AccountInfoResponse response =
			payService.getAccountInfo(request);

		out.println(JsonHelper.formatJson(response));
	}

	public void balance(Options option, String[] args)
		throws Exception
	{
		BalanceRequest request = new BalanceRequest();

		request.setAccountId(option.account);
		
		try
		{
			request.setReason(
				BalanceRequest.Reason.valueOf(option.account));
		} catch(Exception e)
		{ }

		BalanceResponse response = payService.queryBalance(request);
		
		out.println(JsonHelper.formatJson(response));
	}

	public void collect(Options option, String[] args)
		throws Exception
	{
		HostingCollectRequest request = new HostingCollectRequest();
		
		request.setPayAccountId(option.account);
		request.setAmount(option.amount);

		if (StringUtil.isNotBlank(option.reason))
			request.setReason(
				HostingCollectRequest.Reason.valueOf(option.reason));
		
		HostingCollectResponse response =
			payService.hostingCollect(request);
		
		out.println(JsonHelper.formatJson(response));
	}

	public void pay(Options option, String[] args)
		throws Exception
	{
		HostingPayRequest request = new HostingPayRequest();
		
		request.setPayeeAccountId(option.account);
		request.setAmount(option.amount);

		if (StringUtil.isNotBlank(option.reason))
			request.setReason(
					HostingPayRequest.Reason.valueOf(option.reason));

		HostingPayResponse response =
			payService.hostingPay(request);

		out.println(JsonHelper.formatJson(response));
	}

	public static void main(String[] args)
	{
		Options options = new Options();

		CmdLineParser parser = new CmdLineParser(options);

        try
        {
            parser.parseArgument(args);

            Method method = PayMain.class.getMethod(
            	options.method, Options.class, String[].class);

            PayMain payMain = new PayMain();

            method.invoke(payMain, options, args);
        }  catch (Exception e)
        {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);

            return;
        }
	}
}
