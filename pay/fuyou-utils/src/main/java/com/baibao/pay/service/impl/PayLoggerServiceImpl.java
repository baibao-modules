package com.baibao.pay.service.impl;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baibao.pay.BussStatus;
import com.baibao.pay.BussType;
import com.baibao.pay.PayLoggerService;
import com.baibao.pay.fuyou.FuyouPayServiceImpl;

public class PayLoggerServiceImpl
	implements PayLoggerService
{
	private static Logger log = LoggerFactory.getLogger(FuyouPayServiceImpl.class);

	@Override
	public String queryLogField(String tradeNo, String fieldName)
	{
		return null;
	}

	@Override
	public String queryLogJon(String tradeNo)
	{
		return null;
	}

	@Override
	public void log(
		Timestamp timestamp, String tradeNo,
		BussType type, String busDesc, String channel, String custNo,
		String targetNo, String request, String response, BussStatus status)
	{
		log.info(String.format("%d: No.%s, type(%s), %s, channel(%s), custNo(%s), targetNo(%s), status(%s)",
			timestamp.getTime(), tradeNo, type, busDesc, channel, custNo, targetNo, status));
		
		log.info(String.format("%s: %s, %s", tradeNo, request, response));

	}

	@Override
	public void logback(
		Timestamp timestamp, String tradeNo, String response, BussStatus status)
	{

	}
}
