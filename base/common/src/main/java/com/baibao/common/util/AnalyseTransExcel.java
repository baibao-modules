package com.baibao.common.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnalyseTransExcel
{	
	private static final Logger logger =
		LoggerFactory.getLogger(AnalyseTransExcel.class);

	/**
	 * excel 每一个sheet工作表显示的记录数
	 */
	private static int counts = 10000;

	/**
	 * 下载excel
	 * @param request
	 * @param os
	 * @param type
	 */
	@SuppressWarnings(value={"all"})
	public static void downLoadExcel(
		HttpServletRequest request, HttpServletResponse response,
		String filename, List list, Class clazz, Map<String, String> titles)
	{
		logger.info("下载开始");

		//sheet工作表的个数
		int length = 0;
		List result = null; //临时list

		if (list != null)
		{//记录的总个数
			length = list.size();
		}

		//计算出需要几个sheet工作表
		length =(length % counts == 0)? length / counts: (length / counts) + 1;
		
		//要解析的实体类
		Object tempObject = null;
	    try
	    {
	    	//得到需要的实体类型
	    	tempObject = clazz.newInstance();
		} catch(InstantiationException | IllegalAccessException e1)
	    {
			e1.printStackTrace();
			logger.error("下载错误", e1);
		}
	    
		Date tempDate = null;//临时date类型
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		OutputStream os = outStream;

		try
		{
			if (CollectionUtils.isNotEmpty(list))
			{
				//创建一个excel
				HSSFWorkbook workBook=new HSSFWorkbook();
				for(int index = 0; index < length; ++index)
				{
					int tempk = (index + 1) * counts;
					
					if (tempk <= list.size())
					{
						result = new ArrayList(
							list.subList(index * counts, (index + 1) * counts));
					} else
					{
						result = new ArrayList(
							list.subList(index * counts, list.size()));
					}

					//创建测试表,每个sheet的名称需要不同
					HSSFSheet sheet = workBook.createSheet("sheet" + index);
					
					sheet.createFreezePane(0, 1, 0, 1);
					
					//创建标题行
					HSSFRow headRow = sheet.createRow(0);
					//创建单元格列，生成标题行
					int valueIndex = 0;
					for(Map.Entry<String, String> entry: titles.entrySet())
					{  
			             headRow.createCell(valueIndex).setCellValue(entry.getValue());
						 valueIndex++;
			        }  

					HSSFRow row = null;
					//生成每条记录
					for(int jndex = 0; jndex < result.size(); ++jndex)
					{
						sheet.setColumnWidth(jndex, 30 * 256);
						
						tempObject = result.get(jndex);
						row = sheet.createRow(jndex + 1);
						
						if (tempObject != null)
						{
							int keyIndex = 0;
							
							for(String key: titles.keySet())
							{
								Object objValue = null;
								
								if (tempObject instanceof Map)
								{
									objValue = ((Map) tempObject).get(key);
								} else
								{
									objValue = invokeMethod(key, tempObject);
								}
								
								if (objValue != null)
								{
									//如果是时间类型
									if (objValue instanceof Date)
									{
										tempDate = (Date) objValue;
										row.createCell(keyIndex).setCellValue(dateToStr(tempDate));
									} else
									{
										row.createCell(keyIndex).setCellValue(String.valueOf(objValue));
									}
								} else
								{
									row.createCell(keyIndex).setCellValue("");
								}

								keyIndex++;
							}
						} else
						{
							System.out.println("数据为空!");
						}
					}
				}

				workBook.write(os);
			}
		} catch(Exception e)
		{
			e.printStackTrace();
			logger.error("导出失败");
		}

		//浏览器响应处理
		responseProcessing(request, response, filename, outStream);
	}
	
	
	 /**
	  * 响应处理
	  * @param request
	  * @param response
	  * @param filename
	  * @param outStream
	  */
	public static void responseProcessing(
		 HttpServletRequest request, HttpServletResponse response,
		 String filename, ByteArrayOutputStream outStream)
	{
		try
		{
			response.setContentType("application/vnd.ms-excel");

			response.setHeader("Content-Disposition",
				String.format("inline;filename=%s.xls",
					URLEncoder.encode(filename, "utf-8")));

			response.getOutputStream().write(outStream.toByteArray());
			response.flushBuffer();
		} catch(IOException e)
		{
			e.printStackTrace();
	    }
	};
	 	 
	// 调用对象方法
	public static Object invokeMethod(String attriName, Object obj)
		throws Exception
	{
		String methodName = getMethodName(attriName);
		Method method = obj.getClass().getDeclaredMethod(methodName);
		Object value = method.invoke(obj, null);
		return value;
	}

	public static String getMethodName(String attriName)
	{
		String first = attriName.charAt(0) + "";
		first = first.trim().toUpperCase();
		attriName = attriName.substring(1);
		attriName = "get" + first + attriName;
		return attriName;
	}
	
	public static String dateToStr(Date time)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String result = null;
		
		if (time != null)
		{
			result = sdf.format(time);
		} else
		{
			result = sdf.format(new Date());
		}

		return result;
	}
}
