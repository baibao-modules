package com.baibao.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtil {

	/**
	 * 创建zip压缩文件
	 * @param fileName	生成zip包路径及名称
	 * @param FilePath  需要压缩为zip目录
	 * @throws IOException
	 */
	public static void createZip(String fileName, String FilePath)
			throws IOException {
		ZipOutputStream zo = new ZipOutputStream(new FileOutputStream(fileName));
		File file = new File(FilePath);
		if (file.exists() && file.isDirectory()) {
			File[] file1 = file.listFiles();
			for (File file2 : file1) {
				String file2Name = file2.getName();
				byte[] byt = new byte[(int) file2.length()];
				FileInputStream fi = new FileInputStream(file2);
				fi.read(byt);
				zo.putNextEntry(new ZipEntry(file2Name));
				zo.write(byt);
			}
			zo.close();
		}
	}

}
