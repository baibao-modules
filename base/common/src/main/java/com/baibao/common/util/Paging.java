package com.baibao.common.util;

import java.util.List;

/**
 * <p>
 * 版权所有：(C)2013-2018 全资本
 * </p>
 * 
 * @作者：zp
 * @时间：2015-6-3
 * @描述：分页工具类
 */
public class Paging<T> {
	private int pageTotal = 1;// 总页数
	private int pageNo = 1;// 当前页
	private int pageSize = 10;// 每页显示个数
	private int countTotal;// 总数据量
	private List<T> pageList;// 分页列表

	public Paging() {

	}

	public Paging(int cursor, int count) {
		this.pageSize = count;
		this.pageNo = cursor;
	}

	// 开始条数
	public int getBeginWith() {
		return pageNo == 1 ? 0 : (pageNo - 1) * pageSize;
	}

	public int getPageTotal() {
		if (countTotal > 0) {
			this.setPageTotal(this.countTotal % this.pageSize == 0 ? this.countTotal
					/ this.pageSize
					: this.countTotal / this.pageSize + 1);
		}
		return pageTotal;
	}

	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCountTotal() {
		return countTotal;
	}

	public void setCountTotal(int countTotal) {
		this.countTotal = countTotal;
	}

	public List<T> getPageList() {
		return pageList;
	}

	public void setPageList(List<T> pageList) {
		this.pageList = pageList;
	}

}
