package com.baibao.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.CharSet;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontProvider;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

public class HtmlToPdfUtil
{
	public static byte[] createPdf(String html)
		throws IOException, DocumentException
	{
		Charset charset = Charset.forName("utf-8");

		ByteArrayInputStream inputStream =
			new ByteArrayInputStream(html.getBytes(charset));
		ByteArrayOutputStream outputStream =
			new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter pdfwriter =
			PdfWriter.getInstance(document, outputStream);
		pdfwriter.setViewerPreferences(PdfWriter.HideToolbar);

		document.open();

		XMLWorkerHelper.getInstance().parseXHtml(
			pdfwriter, document, inputStream, Charset.forName("utf-8"),
			new FontProvider() {
				private BaseFont baseFont =
					BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H",
						true);

				@Override
				public boolean isRegistered(String fontname)
				{
					return true;
				}

				@Override
				public Font getFont(
					String fontname, String encoding, boolean embedded,
					float size, int style, BaseColor color)
				{
					return new Font(baseFont, size, style, color);
				}
			});

		document.close();

		return outputStream.toByteArray();
	}
}
