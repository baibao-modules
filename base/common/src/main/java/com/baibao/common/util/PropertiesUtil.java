package com.baibao.common.util;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * @author jiangp
 * @date 2015年10月26日
 */
public class PropertiesUtil {

	/**
	 * 短信相关设置
	 */
	private static Configuration sysConfig = null;
	
	
	static {
		try {
			sysConfig = getConfiguration();
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 获取系统配置文件参数
	 * @param paramter
	 * @return
	 */
	public static String getSysConfigParams(String key){
		if(sysConfig == null){
			try {
				sysConfig = getConfiguration();
			} catch (ConfigurationException e) {
				e.printStackTrace();
			}
		}
		return sysConfig.getString(key);
	}
	
	private static PropertiesConfiguration getConfiguration() throws ConfigurationException{
		String path = PropertiesUtil.class.getClassLoader().getResource("/config/properties/sysconfig.properties").getPath();
		return new PropertiesConfiguration(path);
	}
	
}
