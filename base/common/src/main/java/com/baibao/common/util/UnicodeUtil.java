package com.baibao.common.util;

import java.math.BigDecimal;


public class UnicodeUtil {

	/**
	 * 字符串转换unicode
	 */
	public static String stringToUnicode(String string) {
	    StringBuffer unicode = new StringBuffer();
	    for (int i = 0; i < string.length(); i++) {
	        // 取出每一个字符
	        char c = string.charAt(i);
	        // 转换为unicode
	        unicode.append("\\u" + Integer.toHexString(c));
	    }
	    return unicode.toString();
	}
	
	/**
	 * unicode 转字符串
	 */
	public static String unicodeToString(String unicode) {
	    StringBuffer string = new StringBuffer();
	    String[] hex = unicode.split("\\\\u");
	    for (int i = 1; i < hex.length; i++) {
	        // 转换出每一个代码点
	        int data = Integer.parseInt(hex[i], 16);
	        // 追加成string
	        string.append((char) data);
	    }
	    return string.toString();
	}

	public static void main(String agrs[])
	{
		
		String flowno = "3021456222";
		flowno = flowno.substring(0, 3);
		
		Integer a   = new Integer(10);
		Integer b   = new Integer(10);
		//Integer b = 1000;
		System.err.println(flowno);
		
//		BigDecimal amount = new BigDecimal("200000.00");
//		BigDecimal riskManageAmountRate =new BigDecimal("2");//风险备用金
//		riskManageAmountRate = riskManageAmountRate==null? new BigDecimal("0.00"):riskManageAmountRate;
//		BigDecimal riskManageAmount = new BigDecimal("0.00");// 风险备用金
//		if(riskManageAmountRate.compareTo(new BigDecimal("0.00")) == 1){
//			riskManageAmount = Arith.mul(amount, Arith.div(riskManageAmountRate, BigDecimal.valueOf(100), 2));
//		}
//		System.err.println(riskManageAmount);
		
//		String test = "操作失败";
//		 
//	    String unicode = stringToUnicode(test);
//	     
//	    System.out.println(unicode);
//	    System.out.println(unicodeToString(unicode));
	    
	}
}
