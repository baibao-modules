package com.baibao.web.appframework.filter;

import java.util.Collection;
import java.util.LinkedList;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;

public class NullMetadataSource
	implements FilterInvocationSecurityMetadataSource
{
	protected  LinkedList<ConfigAttribute> thisAttrs =
		new LinkedList<ConfigAttribute>();

	public NullMetadataSource()
	{
		thisAttrs.add(new ConfigAttribute() {
			public String getAttribute()
			{
				return "attribute";
			}
		});
	}

	public Collection<ConfigAttribute> getAttributes(Object object)
		throws IllegalArgumentException
	{
		return thisAttrs;
	}

	public Collection<ConfigAttribute> getAllConfigAttributes()
	{
		return thisAttrs;
	}

	public boolean supports(Class<?> clazz)
	{
		return true;
	}

}

