package com.baibao.webbox.advice;

import javax.persistence.PersistenceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;

@Aspect
public class DeadLockRetryAspect
{
	private static Log log = LogFactory.getLog(DeadLockRetryAspect.class);

	@Around(value = "@annotation(org.springframework.transaction.annotation.Transactional)")
	public Object retry(final ProceedingJoinPoint joinPoint)
		throws Throwable
	{
		Object result = null;

		try
		{
			result = joinPoint.proceed();
		} catch(PersistenceException exception) 
		{
			int counter = 0;
			
			log.error(String.format(
				"Deadlock found, catch %s", exception.getCause().getMessage()));
			
			while(isDeadlock(exception) && counter < 5)
			{
				log.error(String.format(
					"Deadlock found, retry %d", counter), exception);

				try
				{
					synchronized(this)
					{
						result = joinPoint.proceed();

						return result;
					}
				} catch(PersistenceException exception1)
				{
					exception = exception1;

					++counter;
				}
			}
		}

		return result;
	}
	
	private Boolean isDeadlock(final PersistenceException exception)
		throws Throwable
	{
		if (exception.getCause() != null &&
			exception.getCause().getMessage().contains("Deadlock found"))
		{
			return true;
		}

		throw exception;
	}
}
