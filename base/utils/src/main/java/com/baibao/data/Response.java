package com.baibao.data;

import java.io.Serializable;

public class Response<T>
	implements Serializable
{
	private static final long serialVersionUID = 2318866198328783813L;

	/**
	 * 状态值
	 */
	private Integer retCode = 200;
	
	/**
	 * 错误信息
	 */
	private String retMsg;
	
	/**
	 * 验证码
	 */
	private String nbCode;
	
	/**
	 * 数据
	 */
	private T result;

	public Integer getRetCode()
	{
		return retCode;
	}

	public void setRetCode(Integer status)
	{
		this.retCode = status;
	}

	public String getRetMsg()
	{
		return retMsg;
	}

	public void setRetMsg(String message)
	{
		this.retMsg = message;
	}

	public T getResult()
	{
		return result;
	}

	public void setResult(T result)
	{
		this.result = result;
	}

	public String getNbCode() {
		return nbCode;
	}

	public void setNbCode(String nbCode) {
		this.nbCode = nbCode;
	}
}
