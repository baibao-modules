package com.baibao.exception;

import java.util.ResourceBundle;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapFault;
import org.apache.cxf.common.i18n.Message;

import com.baibao.data.Response;

public class BusinessException
	extends SoapFault
{
	private static final long serialVersionUID = 1337477185117468301L;

	private int status;
	private String message;
	private String serverMsg;

	public BusinessException(int status, String message, String serverMsg)
	{
		super(new Message(message,(ResourceBundle) null),
			new QName("http://www.marioq.duocai.cn/", "businessException"));

		this.status = status;
		this.message = message;
		this.serverMsg = serverMsg;
		
		this.setRole(String.valueOf(this.status));
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
	
	public String getServerMsg() {
		return serverMsg;
	}

	public void setServerMsg(String serverMsg) {
		this.serverMsg = serverMsg;
	}

	public Response<Object> toResponse()
	{
		Response<Object> response = new Response<Object>();

		response.setRetCode(status);
		response.setRetMsg(message);

		return response;
	}
}
