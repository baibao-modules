@XmlJavaTypeAdapters({
    @XmlJavaTypeAdapter(PageXmlAdapter.class),
    @XmlJavaTypeAdapter(PageableXmlAdapter.class)
})
package com.baibao.utils.service;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import com.baibao.jaxb.*;
