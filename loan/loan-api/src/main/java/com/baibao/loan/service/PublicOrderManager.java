package com.baibao.loan.service;

import javax.jws.WebService;

import java.util.ArrayList;

import javax.jws.WebMethod;

import com.baibao.loan.model.PublicOrder;
import com.baibao.utils.service.GenericManager;

@WebService
public interface PublicOrderManager
    extends GenericManager<PublicOrder, Long>
{
	public ArrayList<PublicOrder> findPublicOrderByStatus(String status);
	
	public ArrayList<PublicOrder>
		findPublicOrderByTypeAndStatus(String[] type, String status);
}



