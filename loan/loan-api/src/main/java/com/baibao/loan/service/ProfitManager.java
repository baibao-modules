package com.baibao.loan.service;

import javax.jws.WebService;

import java.util.ArrayList;

import javax.jws.WebMethod;

import com.baibao.loan.model.Profit;
import com.baibao.utils.service.GenericManager;

@WebService
public interface ProfitManager
    extends GenericManager<Profit, String>
{
	public ArrayList<Profit> findProfits(String productNo, Long seqNum);
}



