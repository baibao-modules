package com.baibao.loan.model;

import com.baibao.loan.model.Profit.Type;

public enum InvestmentType
{
	retail(1, "直投/散标"),
	resell(2, "债转标"),
	pack(3, "资产包");

	InvestmentType(Integer code, String desc)
	{
		this.code = code;
		this.desc = desc;
	}

	private Integer code;
	private String desc;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static Type codeOf(Integer code)
	{
		for(Type type: Type.values())
		{
			if (type.getCode().equals(code))
				return type;
		}

		return null;
	}
}
