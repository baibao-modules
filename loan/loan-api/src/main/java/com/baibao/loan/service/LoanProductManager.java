package com.baibao.loan.service;

import javax.jws.WebService;

import java.sql.Timestamp;
import java.util.ArrayList;

import javax.jws.WebMethod;

import com.baibao.loan.model.LoanProduct;
import com.baibao.utils.service.GenericManager;

@WebService
public interface LoanProductManager
    extends GenericManager<LoanProduct, String>
{
	public ArrayList<LoanProduct>
		findCancelProduct(Timestamp now, Integer status);
}



