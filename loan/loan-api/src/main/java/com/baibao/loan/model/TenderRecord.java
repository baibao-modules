
package com.baibao.loan.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.*;

import org.apache.openjpa.persistence.jdbc.Index;

import com.baibao.loan.model.Profit.Type;

/**
 * 投资记录
 */
@Entity
@Table(name = "p2p_tender_record")
public class TenderRecord
{
	/**
	 * 投资单号
	 */
    @Id
    @Column(name = "ptr_tenderFlowNo")
    private String id;

	/**
	 * 支付账户
	 */
    @Column(name = "ptr_accountNo")
    private String accountNo;

	/**
	 * 产品编号
	 */
    @Column(name = "ptr_productNo")
    private String productNo;

	/**
	 * 用户编号
	 */
    @Column(name = "ptr_custNo")
    private String custNo;

	/**
	 * 投资金额
	 */
    @Column(name = "ptr_amount")
    private BigDecimal amount;

	/**
	 * 投资时间
	 */
    @Column(name = "ptr_transTime")
    private Timestamp transTime;

	/**
	 * 投资类型
	 */
    @Column(name = "ptr_status")
    private String status;

	/**
	 * 交易渠道
	 */
    @Column(name = "ptr_channel")
    private String channel;

	/**
	 * 交易状态
	 */
    @Column(name = "ptr_tradeStatus")
    private String tradeStatus;

	/**
	 * 第三方交易状态码
	 */
    @Column(name = "ptr_payStatusCode")
    private String payStatusCode;

	/**
	 * 第三方响应时间
	 */
    @Column(name = "ptr_host_response_time")
    private Timestamp responseTime;

	/**
	 * 第三方返回流水号
	 */
    @Column(name = "ptr_host_response_flowNo")
    private String responseNo;

	/**
	 * 第三方返回码
	 */
    @Column(name = "ptr_host_return_code")
    private String returnCode;
    
	/**
	 * 第三方返回信息
	 */
    @Column(name = "ptr_host_return_message")
    private String returnMessage;

	/**
	 * 
	 */
    @Column(name = "ptr_dealStatus")
    private String dealStatus;

	/**
	 * 
	 */
    @Column(name = "ptr_backStatus")
    private String backStatus;

	/**
	 * 备注
	 */
    @Column(name = "ptr_remark")
    private String remark;

    public static enum Status
    {
    	normal("10", "正常投资"),
    	resell("11", "正在作为债权转出"),
    	resold("12", "已转出");

    	Status(String code, String desc)
    	{
    		this.code = code;
    		this.desc = desc;
    	}

    	private String code;
    	private String desc;
    	
    	public String getCode() {
    		return code;
    	}
    	public void setCode(String code) {
    		this.code = code;
    	}
    	public String getDesc() {
    		return desc;
    	}
    	public void setDesc(String desc) {
    		this.desc = desc;
    	}

    	public static Type codeOf(String code)
    	{
    		for(Type type: Type.values())
    		{
    			if (type.getCode().equals(code))
    				return type;
    		}

    		return null;
    	}
    }

    public static enum TradeStatus
    {
    	success("00", "成功"),
    	failed("01", "失败"),
    	pending("02", "处理中"),
    	cancel("03", "流标退款");

    	TradeStatus(String code, String desc)
    	{
    		this.code = code;
    		this.desc = desc;
    	}

    	private String code;
    	private String desc;
    	
    	public String getCode() {
    		return code;
    	}
    	public void setCode(String code) {
    		this.code = code;
    	}
    	public String getDesc() {
    		return desc;
    	}
    	public void setDesc(String desc) {
    		this.desc = desc;
    	}

    	public static Type codeOf(String code)
    	{
    		for(Type type: Type.values())
    		{
    			if (type.getCode().equals(code))
    				return type;
    		}

    		return null;
    	}
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getProductNo() {
		return productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Timestamp getTransTime() {
		return transTime;
	}

	public void setTransTime(Timestamp transTime) {
		this.transTime = transTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public String getPayStatusCode() {
		return payStatusCode;
	}

	public void setPayStatusCode(String payStatusCode) {
		this.payStatusCode = payStatusCode;
	}

	public Timestamp getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	public String getResponseNo() {
		return responseNo;
	}

	public void setResponseNo(String responseNo) {
		this.responseNo = responseNo;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(String dealStatus) {
		this.dealStatus = dealStatus;
	}

	public String getBackStatus() {
		return backStatus;
	}

	public void setBackStatus(String backStatus) {
		this.backStatus = backStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}

