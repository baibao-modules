
package com.baibao.loan.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.*;

import org.apache.openjpa.persistence.jdbc.Index;

/**
 * 投资人应收收益
 */
@Entity
@Table(name = "p2p_tender_profit")
public class Profit
{	
	/**
	 * 投资收益ID
	 */
    @Id
    @Column(name = "ptp_incomebillNo")
    private String id;

    /**
     * 投资单号
     */
    @Column(name = "ptp_tenderFlowNo")
    private String tenderFlowNo;

    /**
     * 借款单号
     */
    @Column(name = "ptp_productNo")
    private String productNo;

    /**
     * 期数
     */
    @Column(name = "ptp_loanSeq")
    private Long seqNum;

    /**
     * 预期本期本金
     */
    @Column(name = "ptp_capital")
    private BigDecimal capital;

    /**
     * 预期本期利息
     */
    @Column(name = "ptp_interest")
    private BigDecimal interest;
    
    /**
     * 预期本期本息和
     */
    @Column(name = "ptp_profit")
    private BigDecimal profit;

    /**
     * 实际本期本金
     */
    @Column(name = "ptp_factCapital")
    private BigDecimal factCapital;
    
    /**
     * 实际本期利息
     */
    @Column(name = "ptp_factInterest")
    private BigDecimal factInterest;
    
    /**
     * 实际本期本息和
     */
    @Column(name = "ptp_factProfit")
    private BigDecimal factProfit;

    /**
     * 预期收益时间
     */
    @Column(name = "ptp_profitTime")
    private Timestamp profitTime;
    
    /**
     * 逾期天数
     */
    @Column(name = "ptp_overDueDays")
    private Long overDueDays;

    /**
     * 逾期费用
     */
    @Column(name = "ptp_overDueAmount")
    private BigDecimal overDueAmount;
    
    /**
     * 实际收益时间
     */
    @Column(name = "ptp_factProfitTime")
    private Timestamp factProfitTime;
    
    /**
     * 收益类型
     */
    @Column(name = "ptp_profitType")
    private String type;

    /**
     * 用户编号
     */
    @Column(name = "ptp_custNo")
    private String custNo;

    /**
     * 投资收益状态
     */
    @Column(name = "ptp_profitstatus")
    private String status;

    /**
     * 收益描述
     */
    @Column(name = "ptp_remark")
    private String remark;

	//40 本金  41 利息 42 本息和  43 佣金
	public static enum Type
	{
		capital("40", "本金"),
		interest("41", "利息"),
		profit("42", "本息和"),
		fee("43", "佣金");

		Type(String code, String desc)
		{
			this.code = code;
			this.desc = desc;
		}

		private String code;
		private String desc;
		
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getDesc() {
			return desc;
		}
		public void setDesc(String desc) {
			this.desc = desc;
		}

		public static Type codeOf(String code)
		{
			for(Type type: Type.values())
			{
				if (type.getCode().equals(code))
					return type;
			}

			return null;
		}
	}

	//00已收益  01未收益 02处理中 5放款中
	public static enum Status
	{
		done("00", "已收益"),
		normal("41", "未收益"),
		procssing("42", "处理中"),
		transfering("43", "放款中");

		Status(String code, String desc)
		{
			this.code = code;
			this.desc = desc;
		}

		private String code;
		private String desc;
		
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getDesc() {
			return desc;
		}
		public void setDesc(String desc) {
			this.desc = desc;
		}

		public static Type codeOf(String code)
		{
			for(Type type: Type.values())
			{
				if (type.getCode().equals(code))
					return type;
			}

			return null;
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTenderFlowNo() {
		return tenderFlowNo;
	}

	public void setTenderFlowNo(String tenderFlowNo) {
		this.tenderFlowNo = tenderFlowNo;
	}

	public String getProductNo() {
		return productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public Long getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(Long seqNum) {
		this.seqNum = seqNum;
	}

	public BigDecimal getCapital() {
		return capital;
	}

	public void setCapital(BigDecimal capital) {
		this.capital = capital;
	}

	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	public BigDecimal getProfit() {
		return profit;
	}

	public void setProfit(BigDecimal profit) {
		this.profit = profit;
	}

	public BigDecimal getFactCapital() {
		return factCapital;
	}

	public void setFactCapital(BigDecimal factCapital) {
		this.factCapital = factCapital;
	}

	public BigDecimal getFactInterest() {
		return factInterest;
	}

	public void setFactInterest(BigDecimal factInterest) {
		this.factInterest = factInterest;
	}

	public BigDecimal getFactProfit() {
		return factProfit;
	}

	public void setFactProfit(BigDecimal factProfit) {
		this.factProfit = factProfit;
	}

	public Timestamp getProfitTime() {
		return profitTime;
	}

	public void setProfitTime(Timestamp profitTime) {
		this.profitTime = profitTime;
	}

	public Timestamp getFactProfitTime() {
		return factProfitTime;
	}

	public void setFactProfitTime(Timestamp factProfitTime) {
		this.factProfitTime = factProfitTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getOverDueDays() {
		return overDueDays;
	}

	public void setOverDueDays(Long overDueDays) {
		this.overDueDays = overDueDays;
	}

	public BigDecimal getOverDueAmount() {
		return overDueAmount;
	}

	public void setOverDueAmount(BigDecimal overDueAmount) {
		this.overDueAmount = overDueAmount;
	}
}

