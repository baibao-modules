
package com.baibao.loan.model;

import javax.persistence.*;

import org.apache.openjpa.persistence.jdbc.Index;

@Table(name = "p2p_bank_card")
@Entity
public class BankCard
{
    @Id
    @Column(name = "pbc_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pbc_custNo")
    private String custNo;

    @Column(name = "pbc_accountNo")
    private String accountNo;
    
    @Column(name = "pbc_bankCardNo")
    private String cardNo;
    
    @Column(name = "pbc_pbcBankName")
    private String bankName;
    
    @Column(name = "pbc_bank_branch")
    private String branchName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
}

