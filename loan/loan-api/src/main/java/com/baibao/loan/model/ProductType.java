package com.baibao.loan.model;

import com.baibao.loan.model.Profit.Type;

public enum ProductType
{
	credit("0", "信用标"),
	pledge("1", "抵押标"),
	financing("2", "融资租赁标");

	ProductType(String code, String desc)
	{
		this.code = code;
		this.desc = desc;
	}

	private String code;
	private String desc;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static Type codeOf(String code)
	{
		for(Type type: Type.values())
		{
			if (type.getCode().equals(code))
				return type;
		}

		return null;
	}
}
