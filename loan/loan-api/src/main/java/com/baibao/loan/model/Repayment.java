
package com.baibao.loan.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.*;

import org.apache.openjpa.persistence.jdbc.Index;

import com.baibao.loan.model.Profit.Type;

/**
 * 借款人应还款项
 */
@Entity
@Table(name = "p2p_repayment_bill")
public class Repayment
{
	/**
	 * 账单编号
	 */
    @Id
    @Column(name = "prb_repaybillNo")
    private String id;

    /**
     * 客户编号
     */
    @Column(name = "prb_custNo")
    private String custNo;

    /**
     * 借款编号
     */
    @Column(name = "prb_productNo")
    private String productNo;

    /**
     * 还款期数
     */
    @Column(name = "prb_repaymentSeq")
    private Long seqNum;

    /**
     * 合约还款日期
     */
    @Column(name = "prb_repaymentTime")
    private Timestamp repaymentTime;

    /**
     * 应还本金
     */
    @Column(name = "prb_repaymentCapital")
    private BigDecimal capital;

    /**
     * 应还利息
     */
    @Column(name = "prb_repaymentInterest")
    private BigDecimal interest;

    
    /**
     * 应还总额
     */
    @Column(name = "prb_repaymentPrincipal")
    private BigDecimal principal;

    /**
     * 实际还款总额
     */
    @Column(name = "prb_infactRepaymentPricipal")
    private BigDecimal infactPricipal;
    
    /**
     * 实际还款日期
     */
    @Column(name = "prb_infactRepaymentTime")
    private Timestamp infactRepaymentTime;

    /**
     * 逾期天数
     */
    @Column(name = "prb_overDueDays")
    private Long overDueDays;

    /**
     * 逾期费用
     */
    @Column(name = "prb_overDueAmount")
    private BigDecimal overDueAmount;
    
    /**
     * 逾期催收费用
     */
    @Column(name = "prb_overDuePunishScore")
    private BigDecimal overDuePunishScore;

    /**
     * 还款说明备注
     */
    @Column(name = "prb_overDueNote")
    private String overDueNote;

    /**
     * 还款状态
     */
    @Column(name = "prb_repaymentStatus")
    private Integer status;

	//0未还款，1正常还款，2逾期，
	//3提前还款, 4处理中的 5放款中
	public static enum Status
	{
		normal(0, "未还款"),
		paid(1, "已还款"),
		overDue(2, "逾期"),
		aheadPaid(3, "提前还款"),
		processing(4, "处理中"),
		transfering(5, "转帐中");

		Status(Integer code, String desc)
		{
			this.code = code;
			this.desc = desc;
		}

		private Integer code;
		private String desc;
		
		public Integer getCode() {
			return code;
		}
		public void setCode(Integer code) {
			this.code = code;
		}
		public String getDesc() {
			return desc;
		}
		public void setDesc(String desc) {
			this.desc = desc;
		}

		public static Type codeOf(Integer code)
		{
			for(Type type: Type.values())
			{
				if (type.getCode().equals(code))
					return type;
			}

			return null;
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getProductNo() {
		return productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public Long getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(Long seqNum) {
		this.seqNum = seqNum;
	}

	public Timestamp getRepaymentTime() {
		return repaymentTime;
	}

	public void setRepaymentTime(Timestamp repaymentTime) {
		this.repaymentTime = repaymentTime;
	}

	public BigDecimal getCapital() {
		return capital;
	}

	public void setCapital(BigDecimal capital) {
		this.capital = capital;
	}

	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	public BigDecimal getPrincipal() {
		return principal;
	}

	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}

	public BigDecimal getInfactPricipal() {
		return infactPricipal;
	}

	public void setInfactPricipal(BigDecimal infactPricipal) {
		this.infactPricipal = infactPricipal;
	}

	public Timestamp getInfactRepaymentTime() {
		return infactRepaymentTime;
	}

	public void setInfactRepaymentTime(Timestamp infactRepaymentTime) {
		this.infactRepaymentTime = infactRepaymentTime;
	}

	public Long getOverDueDays() {
		return overDueDays;
	}

	public void setOverDueDays(Long overDueDays) {
		this.overDueDays = overDueDays;
	}

	public BigDecimal getOverDueAmount() {
		return overDueAmount;
	}

	public void setOverDueAmount(BigDecimal overDueAmount) {
		this.overDueAmount = overDueAmount;
	}

	public BigDecimal getOverDuePunishScore() {
		return overDuePunishScore;
	}

	public void setOverDuePunishScore(BigDecimal overDuePunishScore) {
		this.overDuePunishScore = overDuePunishScore;
	}

	public String getOverDueNote() {
		return overDueNote;
	}

	public void setOverDueNote(String overDueNote) {
		this.overDueNote = overDueNote;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}

