package com.baibao.loan.service;

import javax.jws.WebService;
import javax.jws.WebMethod;

import com.baibao.loan.model.BankCard;
import com.baibao.utils.service.GenericManager;

@WebService
public interface BankCardManager
    extends GenericManager<BankCard, Long>
{
	public int updateBankCard(String accountNo, String bankName, String cardNo);
}



