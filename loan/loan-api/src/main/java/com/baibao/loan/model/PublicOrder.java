
package com.baibao.loan.model;

import java.sql.Timestamp;

import javax.persistence.*;

import org.apache.openjpa.persistence.jdbc.Index;

@Table(name = "p2p_public_order")
@Entity
public class PublicOrder
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ppo_orderFlowNo")
    private String flowNo;
    
    @Column(name = "ppo_custNo")
    private String custNo;
    
    @Column(name = "ppo_targetNo")
    private String targetNo;
    
    @Column(name = "ppo_buss_type")
    private String bussType;
    
    @Column(name = "ppo_buss_desc")
    private String bussDesc;
    
    @Column(name = "ppo_order_time")
    private Timestamp orderTime;

    @Column(name = "ppo_accountChannel")
    private String channel;

    @Column(name = "ppo_status")
    private String status;

    @Column(name = "ppo_params")
    private String params;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getFlowNo() {
		return flowNo;
	}

	public void setFlowNo(String flowNo) {
		this.flowNo = flowNo;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getTargetNo() {
		return targetNo;
	}

	public void setTargetNo(String targetNo) {
		this.targetNo = targetNo;
	}

	public String getBussType() {
		return bussType;
	}

	public void setBussType(String bussType) {
		this.bussType = bussType;
	}

	public String getBussDesc() {
		return bussDesc;
	}

	public void setBussDesc(String bussDesc) {
		this.bussDesc = bussDesc;
	}

	public Timestamp getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Timestamp orderTime) {
		this.orderTime = orderTime;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public static enum BussType
	{
		other(-1, "其它"),
		bindCard(8, "绑卡"),
		recharge(10, "充值"),
		withdraw(11, "提现"),
		tender(12, "投资"),
		collecting(13, "收款"),
		pay(14, "付款");

		BussType(Integer code, String desc)
		{
			this.code = code;
			this.desc = desc;
		}

		private Integer code;
		private String desc;

		public Integer getCode() {
			return code;
		}
		public void setCode(Integer code) {
			this.code = code;
		}
		public String getDesc() {
			return desc;
		}
		public void setDesc(String desc) {
			this.desc = desc;
		}
	}
	
	public static enum Status
	{
		success("00", "成功"),
		failed("01", "失败"),
		process("02", "处理中");

		Status(String code, String desc)
		{
			this.code = code;
			this.desc = desc;
		}

		private String code;
		private String desc;

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
	}
}

