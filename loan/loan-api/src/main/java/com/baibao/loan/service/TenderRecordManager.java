package com.baibao.loan.service;

import javax.jws.WebService;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.jws.WebMethod;

import com.baibao.loan.model.TenderRecord;
import com.baibao.utils.service.GenericManager;

@WebService
public interface TenderRecordManager
    extends GenericManager<TenderRecord, String>
{
	public TenderRecord tender(
		String productNo, String accountNo,
		String customerNo, String channel, BigDecimal amount);

	public ArrayList<TenderRecord>
		findByProductNoAndStatus(String productNo, String status);
}
