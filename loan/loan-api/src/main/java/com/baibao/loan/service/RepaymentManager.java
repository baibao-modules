package com.baibao.loan.service;

import javax.jws.WebService;

import java.sql.Timestamp;
import java.util.ArrayList;

import javax.jws.WebMethod;

import com.baibao.loan.model.Repayment;
import com.baibao.utils.service.GenericManager;

@WebService
public interface RepaymentManager
    extends GenericManager<Repayment, String>
{
	public ArrayList<Repayment>
		findOverdueRepayment(Integer[] status);

	public ArrayList<Repayment>
		findOverdueRepayment(Integer[] status, Timestamp now);
}



