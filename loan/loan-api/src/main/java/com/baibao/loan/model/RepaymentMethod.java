package com.baibao.loan.model;

import com.baibao.loan.model.Profit.Type;

public enum RepaymentMethod
{
	fixed("0", "等额本息"),
	fixedInterest("1", "先息后本"),
	fixedDaily("2", "按天计息"),
	fixedBasis("3", "等额本金");

	RepaymentMethod(String code, String desc)
	{
		this.code = code;
		this.desc = desc;
	}

	private String code;
	private String desc;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static Type codeOf(String code)
	{
		for(Type type: Type.values())
		{
			if (type.getCode().equals(code))
				return type;
		}

		return null;
	}
}
