
package com.baibao.loan.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.*;

import org.apache.openjpa.persistence.jdbc.Index;

import com.baibao.loan.model.Profit.Type;

/**
 * 投资产品
 */
@Entity
@Table(name = "p2p_loan_product")
public class LoanProduct
{
	/**
	 * 产品编号(与借款申请表loanNo一致)
	 */
    @Id
    @Column(name = "pp_productNo")
    private String id;

    /**
     * 用户编号
     */
    @Column(name = "pp_custNo")
    private String custNo;

    /**
     * 借款标题
     */
    @Column(name = "pp_loanTitle")
    private String title;

    /**
     * 借款期限
     */
    @Column(name = "pp_loanTerm")
    private Integer term;

    /**
     * 借款目标金额
     */
    @Column(name = "pp_loanAmount")
    private BigDecimal amount;

    /**
     * 已融资金额
     */
    @Column(name = "pp_loanAmountExist")
    private BigDecimal existsAmount;

    /**
     * 借款年利率
     */
    @Column(name = "pp_comprehensiveRate")
    private Double comprehensiveRate;

    /**
     * 还款方式
     */
    @Column(name = "pp_loanRepayment")
    private Integer repaymentMethod;

    /**
     * 应还款总额
     */
    @Column(name = "pp_repaymentAmount")
    private BigDecimal repaymentAmount;
    
    /**
     * 产品类型
     */
    @Column(name = "pp_productTypeNo")
    private String type;

    /**
     * 融资状态
     */
    @Column(name = "pp_bulkLoanStatus")
    private Integer status;

    /**
     * 预定投标开始时间
     */
    @Column(name = "pp_bulkLoanStartTime")
    private Timestamp startTime;

    /**
     * 预定投标结束时间
     */
    @Column(name = "pp_bulkLoanEndTime")
    private Timestamp endTime;

    /**
     * 实际满标时间
     */
    @Column(name = "pp_bulkLoanEffectTime")
    private Timestamp effectTime;

    /**
     * 成交用时
     */
    @Column(name = "pp_bulkLoanDealSeconds")
    private String dealSeconds;

    /**
     * 最小投标金额
     */
    @Column(name = "pp_terderMinAmount")
    private BigDecimal minAmount;
    
    /**
     * 最大投标金额
     */
    @Column(name = "pp_tenderMaxAmount")
    private BigDecimal maxAmount;
    
    /**
     * 递增金额
     */
    @Column(name = "pp_tenderIncreaseAmount")
    private BigDecimal increaseAmount;

    /**
     * 产品活动类型
     */
    @Column(name = "pp_activityType")
    private Integer activityType;
    
    /**
     * 投资类型
     */
    @Column(name = "pp_investmentType")
    private Integer investmentType;

    /**
     * 数据保全是否全部上传成功
     */
    @Column(name = "pp_preservationState")
    private String preservationState;

    
    public static enum Status
    {
    	init(0, "初始"),
    	normal(1, "开放投标"),
    	full(2, "满标"),
    	cancel(3, "流标"),
    	repaying(4, "还款中"),
    	closed(5, "结束");

    	Status(Integer code, String desc)
    	{
    		this.code = code;
    		this.desc = desc;
    	}

    	private Integer code;
    	private String desc;
    	
    	public Integer getCode() {
    		return code;
    	}
    	public void setCode(Integer code) {
    		this.code = code;
    	}
    	public String getDesc() {
    		return desc;
    	}
    	public void setDesc(String desc) {
    		this.desc = desc;
    	}

    	public static Type codeOf(Integer code)
    	{
    		for(Type type: Type.values())
    		{
    			if (type.getCode().equals(code))
    				return type;
    		}

    		return null;
    	}
    }
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getExistsAmount() {
		return existsAmount;
	}

	public void setExistsAmount(BigDecimal existsAmount) {
		this.existsAmount = existsAmount;
	}

	public Double getComprehensiveRate() {
		return comprehensiveRate;
	}

	public void setComprehensiveRate(Double comprehensiveRate) {
		this.comprehensiveRate = comprehensiveRate;
	}

	public Integer getRepaymentMethod() {
		return repaymentMethod;
	}

	public void setRepaymentMethod(Integer repaymentMethod) {
		this.repaymentMethod = repaymentMethod;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public Timestamp getEffectTime() {
		return effectTime;
	}

	public void setEffectTime(Timestamp effectTime) {
		this.effectTime = effectTime;
	}

	public String getDealSeconds() {
		return dealSeconds;
	}

	public void setDealSeconds(String dealSeconds) {
		this.dealSeconds = dealSeconds;
	}

	public BigDecimal getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}

	public BigDecimal getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}

	public Integer getActivityType() {
		return activityType;
	}

	public void setActivityType(Integer activityType) {
		this.activityType = activityType;
	}

	public Integer getInvestmentType() {
		return investmentType;
	}

	public void setInvestmentType(Integer investmentType) {
		this.investmentType = investmentType;
	}

	public String getPreservationState() {
		return preservationState;
	}

	public void setPreservationState(String preservationState) {
		this.preservationState = preservationState;
	}

	public BigDecimal getRepaymentAmount() {
		return repaymentAmount;
	}

	public void setRepaymentAmount(BigDecimal repaymentAmount) {
		this.repaymentAmount = repaymentAmount;
	}
}

