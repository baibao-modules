package com.baibao.loan.service.impl;

import java.util.ArrayList;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.loan.model.BankCard;
import com.baibao.loan.repository.BankCardRepository;
import com.baibao.loan.service.BankCardManager;
import com.baibao.utils.Utils;
import com.baibao.utils.service.AbstractManagerImpl;

@WebService(serviceName = "bankCardManager")
@Service("bankCardManager")
public class BankCardManagerImpl
    extends AbstractManagerImpl<BankCard, Long>
    implements BankCardManager
{
    @Autowired
    protected BankCardRepository bankCardRepository;

    public void afterPropertiesSet() throws Exception {
        setRepository(bankCardRepository);
    }

	@Override
	@Transactional
	public int updateBankCard(String accountNo, String bankName, String cardNo)
	{
		return bankCardRepository.updateBankCard(accountNo, bankName, cardNo);
	}
}



