package com.baibao.loan.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.baibao.loan.model.BankCard;

@Repository
public interface BankCardRepository
    extends JpaRepository<BankCard, Long>, JpaSpecificationExecutor<BankCard>
{
	@Modifying
	@Query("UPDATE BankCard bc SET bc.bankName = ?2, bc.cardNo = ?3 WHERE bc.accountNo = ?1")
	public int updateBankCard(String accountNo, String bankName, String cardNo);
}
