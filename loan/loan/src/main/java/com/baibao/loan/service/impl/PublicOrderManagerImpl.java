package com.baibao.loan.service.impl;

import java.util.ArrayList;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.loan.model.PublicOrder;
import com.baibao.loan.repository.PublicOrderRepository;
import com.baibao.loan.service.PublicOrderManager;
import com.baibao.utils.Utils;
import com.baibao.utils.service.AbstractManagerImpl;

@WebService(serviceName = "publicOrderManager")
@Service("publicOrderManager")
public class PublicOrderManagerImpl
    extends AbstractManagerImpl<PublicOrder, Long>
    implements PublicOrderManager
{
    @Autowired
    protected PublicOrderRepository publicOrderRepository;

    public void afterPropertiesSet() throws Exception {
        setRepository(publicOrderRepository);
    }

	@Override
	public ArrayList<PublicOrder> findPublicOrderByStatus(String status)
	{
		return publicOrderRepository.findPublicOrderByStatus(status);
	}

	@Override
	public ArrayList<PublicOrder>
		findPublicOrderByTypeAndStatus(String[] type, String status)
	{
		return publicOrderRepository
			.findPublicOrderByTypeAndStatus(type, status);
	}
}
