package com.baibao.loan.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.baibao.loan.model.Profit;

@Repository
public interface ProfitRepository
    extends JpaRepository<Profit, String>, JpaSpecificationExecutor<Profit>
{
	@Query("SELECT p FROM Profit p WHERE p.productNo = ?1 AND p.seqNum = ?2")
	public ArrayList<Profit> findProfits(String productNo, Long seqNum);
}



