package com.baibao.loan.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.loan.model.Repayment;
import com.baibao.loan.repository.RepaymentRepository;
import com.baibao.loan.service.RepaymentManager;
import com.baibao.utils.Utils;
import com.baibao.utils.service.AbstractManagerImpl;

@WebService(serviceName = "repaymentManager")
@Service("repaymentManager")
public class RepaymentManagerImpl
    extends AbstractManagerImpl<Repayment, String>
    implements RepaymentManager
{
    @Autowired
    protected RepaymentRepository repaymentRepository;

    public void afterPropertiesSet() throws Exception {
        setRepository(repaymentRepository);
    }

	@Override
	public ArrayList<Repayment>
		findOverdueRepayment(Integer[] status, Timestamp now)
	{
		ArrayList<Repayment> repayments =
			repaymentRepository.findOverdueRepayment(status, now);

		return repayments;
	}

	@Override
	public ArrayList<Repayment> findOverdueRepayment(Integer[] status)
	{
		ArrayList<Repayment> repayments =
			repaymentRepository.findOverdueRepayment(status);

		return repayments;
	}
}



