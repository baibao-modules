package com.baibao.loan.repository;

import java.sql.Timestamp;
import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.baibao.loan.model.LoanProduct;

@Repository
public interface LoanProductRepository
    extends JpaRepository<LoanProduct, String>,
    	JpaSpecificationExecutor<LoanProduct>
{
	@Query("SELECT lp FROM LoanProduct lp WHERE " +
		"lp.endTime <= ?1 AND lp.status = ?2")
	public ArrayList<LoanProduct>
		findCancelProduct(Timestamp now, Integer status);
}

