package com.baibao.loan.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.baibao.loan.model.TenderRecord;

@Repository
public interface TenderRecordRepository
    extends JpaRepository<TenderRecord, String>,
    	JpaSpecificationExecutor<TenderRecord>
{
	@Query("SELECT tr FROM TenderRecord tr WHERE " +
		"tr.productNo = ?1 AND tr.status = ?2")
	public ArrayList<TenderRecord>
		findByProductNoAndStatus(String productNo, String status);
}



