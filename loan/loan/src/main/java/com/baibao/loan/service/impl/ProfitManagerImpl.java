package com.baibao.loan.service.impl;

import java.util.ArrayList;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.loan.model.Profit;
import com.baibao.loan.repository.ProfitRepository;
import com.baibao.loan.service.ProfitManager;
import com.baibao.utils.Utils;
import com.baibao.utils.service.AbstractManagerImpl;

@WebService(serviceName = "profitManager")
@Service("profitManager")
public class ProfitManagerImpl
    extends AbstractManagerImpl<Profit, String>
    implements ProfitManager
{
    @Autowired
    protected ProfitRepository profitRepository;

    public void afterPropertiesSet() throws Exception {
        setRepository(profitRepository);
    }

	@Override
	public ArrayList<Profit>
		findProfits(String productNo, Long seqNum)
	{
		ArrayList<Profit> profits =
			profitRepository.findProfits(productNo, seqNum);

		return profits;
	}
}



