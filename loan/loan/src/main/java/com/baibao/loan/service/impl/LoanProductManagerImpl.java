package com.baibao.loan.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.loan.model.LoanProduct;
import com.baibao.loan.repository.LoanProductRepository;
import com.baibao.loan.service.LoanProductManager;
import com.baibao.utils.Utils;
import com.baibao.utils.service.AbstractManagerImpl;

@WebService(serviceName = "loanProductManager")
@Service("loanProductManager")
public class LoanProductManagerImpl
    extends AbstractManagerImpl<LoanProduct, String>
    implements LoanProductManager
{
    @Autowired
    protected LoanProductRepository loanProductRepository;

    public void afterPropertiesSet() throws Exception {
        setRepository(loanProductRepository);
    }

	@Override
	public ArrayList<LoanProduct>
		findCancelProduct(Timestamp now, Integer status)
	{
		return loanProductRepository.findCancelProduct(now, status);
	}
}
