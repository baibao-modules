package com.baibao.loan.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.baibao.loan.model.PublicOrder;

@Repository
public interface PublicOrderRepository
    extends JpaRepository<PublicOrder, Long>,
    	JpaSpecificationExecutor<PublicOrder>
{
	@Query("SELECT po FROM PublicOrder po WHERE po.status = ?1")
	public ArrayList<PublicOrder> findPublicOrderByStatus(String status);
	
	@Query("SELECT po FROM PublicOrder po WHERE " +
		"po.bussType IN (?1) AND po.status = ?2")
	public ArrayList<PublicOrder>
		findPublicOrderByTypeAndStatus(String[] type, String status);
}
