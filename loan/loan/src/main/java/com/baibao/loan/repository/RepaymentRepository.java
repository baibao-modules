package com.baibao.loan.repository;

import java.sql.Timestamp;
import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.baibao.loan.model.Repayment;

@Repository
public interface RepaymentRepository
    extends JpaRepository<Repayment, String>, JpaSpecificationExecutor<Repayment>
{
	@Query("SELECT r FROM Repayment r WHERE r.status IN (?1) AND r.repaymentTime < ?2")
	public ArrayList<Repayment>
		findOverdueRepayment(Integer[] status, Timestamp now);

	@Query("SELECT r FROM Repayment r WHERE r.status IN (?1)")
	public ArrayList<Repayment>
		findOverdueRepayment(Integer[] status);
}



