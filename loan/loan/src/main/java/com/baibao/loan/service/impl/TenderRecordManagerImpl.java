package com.baibao.loan.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.cache.utils.CacheUtils;
import com.baibao.exception.BusinessException;
import com.baibao.loan.model.LoanProduct;
import com.baibao.loan.model.TenderRecord;
import com.baibao.loan.repository.TenderRecordRepository;
import com.baibao.loan.service.LoanProductManager;
import com.baibao.loan.service.TenderRecordManager;
import com.baibao.pay.GetUniqueNoService;
import com.baibao.utils.Utils;
import com.baibao.utils.service.AbstractManagerImpl;

@WebService(serviceName = "tenderRecordManager")
@Service("tenderRecordManager")
public class TenderRecordManagerImpl
    extends AbstractManagerImpl<TenderRecord, String>
    implements TenderRecordManager
{
    @Autowired
    protected TenderRecordRepository tenderRecordRepository;
    
    @Autowired
    protected LoanProductManager loanProductManager;

    @Autowired
    protected GetUniqueNoService getUniqueNoService;
    
    @Autowired
    protected RedisTemplate<String, String> redisTemplate;

    public void afterPropertiesSet()
    	throws Exception
    {
        setRepository(tenderRecordRepository);
    }

	@Override
	@Transactional
	public TenderRecord tender(
		final String productNo, final String accountNo, final String customerNo,
		final String channel, final BigDecimal amount)
	{
		final TenderRecord tenderRecord = new TenderRecord();

		CacheUtils.lockSpinning(new Runnable() {
			@Override
			public void run()
			{
				final LoanProduct loanProduct = loanProductManager.get(productNo);

				if (loanProduct == null)
					throw new BusinessException(
						404, "投资产品不存在", "not found");

				BigDecimal remain =
					loanProduct.getAmount()
						.subtract(loanProduct.getExistsAmount());
				if (remain.compareTo(amount) < 0)
					throw new BusinessException(
						401, "投资产品可投资余额不足", "no amount");

				tenderRecord.setId(getUniqueNoService.getBidRTransFlow());
				tenderRecord.setAccountNo(accountNo);
				tenderRecord.setProductNo(productNo);
				tenderRecord.setCustNo(customerNo);
				tenderRecord.setAmount(amount);
				tenderRecord.setTransTime(Utils.now());
				tenderRecord.setStatus(
					TenderRecord.TradeStatus.pending.getCode());
				tenderRecord.setChannel(channel);
				tenderRecord.setDealStatus("00");

				tenderRecordRepository.save(tenderRecord);

				loanProduct.setExistsAmount(
					loanProduct.getExistsAmount().add(amount));

				if (loanProduct.getExistsAmount()
						.compareTo(loanProduct.getAmount()) == 0)
				{
					loanProduct.setStatus(LoanProduct.Status.full.getCode());
				}

				loanProductManager.save(loanProduct);
			}
		}, redisTemplate,
		String.format("product.%s", productNo),
		String.format("account.%s", customerNo),
		60l, TimeUnit.SECONDS, 120 * Utils.SECONDS);

		return tenderRecord;
	}

	@Override
	public ArrayList<TenderRecord>
		findByProductNoAndStatus(String productNo, String status)
	{
		return tenderRecordRepository
			.findByProductNoAndStatus(productNo, status);
	}
}



