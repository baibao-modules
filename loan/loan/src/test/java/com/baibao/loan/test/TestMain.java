package com.baibao.loan.test;

import java.lang.reflect.Field;

import com.baibao.enums.EnumHelper;
import com.baibao.enums.Ordinal;

public class TestMain
{
	public static enum Test
	{		
		@Ordinal(12)
		test1,
		@Ordinal(13)
		test2;

		static {
			EnumHelper.setOrdinal(Test.class);
		}
	}

	public static void setOrdinal(Enum object, int value)
	{
		try
		{
			Field field =
				object.getClass().getSuperclass().getDeclaredField("ordinal");
			
			field.setAccessible(true);
			field.set(object, value);
		} catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		
		System.out.println(Test.test1.ordinal());
	}
}
