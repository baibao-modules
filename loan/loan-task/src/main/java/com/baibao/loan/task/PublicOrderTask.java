package com.baibao.loan.task;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.baibao.loan.model.PublicOrder;
import com.baibao.loan.service.PublicOrderManager;
import com.baibao.loan.task.service.LoanTaskService;

public class PublicOrderTask
{
	private static Log log = LogFactory.getLog(PublicOrderTask.class);

	@Autowired
	protected PublicOrderManager publicOrderManager;
	
	@Autowired
	protected LoanTaskService loanTaskService;

	public void checkPublicOrder()
	{
		log.info("start check public order");

		ArrayList<PublicOrder> orders =
			publicOrderManager.findPublicOrderByStatus(
				PublicOrder.Status.process.getCode());
		
		log.info(String.format("%d public order found", orders.size()));
		
		for(PublicOrder order: orders)
		{
			loanTaskService.processPublicOrder(order);
		}
		
		log.info("end check public order");
	}
}
