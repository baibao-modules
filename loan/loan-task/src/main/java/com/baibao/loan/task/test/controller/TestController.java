package com.baibao.loan.task.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baibao.data.Response;
import com.baibao.loan.task.OverdueTask;
import com.baibao.loan.task.PublicOrderTask;

@RequestMapping("/test")
@Controller
public class TestController
{
	@Autowired
	protected OverdueTask overdueTask;
	
	@Autowired
	protected PublicOrderTask publicOrderTask;

	@ResponseBody
	@RequestMapping("/checkOverdue.fn")
	public Response checkOverdue()
	{
		Response response = new Response();
		
		try
		{
			overdueTask.checkOverdue();
		} catch(Exception e)
		{
			response.setRetCode(500);
			response.setRetMsg(e.toString());
		}
				
		return response;
	}

	@ResponseBody
	@RequestMapping("/checkPublic.fn")
	public Response checkPublic()
	{
		Response response = new Response();
		
		try
		{
			publicOrderTask.checkPublicOrder();
		} catch(Exception e)
		{
			response.setRetCode(500);
			response.setRetMsg(e.toString());
		}
				
		return response;
	}

	@ResponseBody
	@RequestMapping("/checkRepayment.fn")
	public Response checkRepayment()
	{
		Response response = new Response();
		
		try
		{
			overdueTask.checkRepayment();
		} catch(Exception e)
		{
			response.setRetCode(500);
			response.setRetMsg(e.toString());
		}
				
		return response;
	}
}
