package com.baibao.loan.task;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.baibao.loan.model.Repayment;
import com.baibao.loan.service.RepaymentManager;
import com.baibao.loan.task.service.LoanTaskService;
import com.baibao.utils.Utils;

public class OverdueTask
{
	private static Log log = LogFactory.getLog(OverdueTask.class);

	@Autowired
	protected RepaymentManager repaymentManager;
	
	@Autowired
	protected LoanTaskService loanTaskService;

	public void checkOverdue()
	{
		log.info("start check overdue repayment");

		ArrayList<Repayment> repayments =
			repaymentManager.findOverdueRepayment(new Integer[] {
				Repayment.Status.normal.getCode(),
				Repayment.Status.overDue.getCode()
			}, Utils.today());

		log.info(String.format("%d repayment found", repayments.size()));

		for(Repayment repayment: repayments)
		{
			try
			{
				loanTaskService.processOverdue(repayment);
			} catch(Exception e)
			{
				log.error(String.format(
					"overdue repayment %s error", repayment.getId()), e);
			}
		}

		log.info("check overdue repayment over");
	}
	
	public void checkRepayment()
	{
		log.info("start check sms repayment");

		ArrayList<Repayment> repayments =
			repaymentManager.findOverdueRepayment(new Integer[] {
				Repayment.Status.normal.getCode(),
				Repayment.Status.overDue.getCode()
			});

		log.info(String.format("%d repayment found", repayments.size()));
		
		for(Repayment repayment: repayments)
		{
			try
			{
				loanTaskService.sendSmsRepayment(repayment);
			} catch(Exception e)
			{
				log.error(String.format(
					"send sms repayment %s error", repayment.getId()), e);
			}
		}
		
		log.info("check sms repayment over");
	}
}
