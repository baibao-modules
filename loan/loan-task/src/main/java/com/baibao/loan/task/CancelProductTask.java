package com.baibao.loan.task;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.baibao.loan.model.LoanProduct;
import com.baibao.loan.model.TenderRecord;
import com.baibao.loan.service.LoanProductManager;
import com.baibao.loan.service.TenderRecordManager;
import com.baibao.loan.task.service.LoanTaskService;
import com.baibao.utils.Utils;

public class CancelProductTask
{
	private static Log log = LogFactory.getLog(CancelProductTask.class);

	@Autowired
	protected LoanProductManager loanProductManager;
	
	@Autowired
	protected LoanTaskService loanTaskService;

	@Autowired
	protected TenderRecordManager tenderRecordManager;
	
	public void checkCancelProduct()
	{
		log.info("start checkCancelProduct");

		ArrayList<LoanProduct> products =
			loanProductManager.findCancelProduct(
				Utils.now(), LoanProduct.Status.normal.getCode());

		log.info(String.format("%d products found", products.size()));

		for(LoanProduct product: products)
		{
			ArrayList<TenderRecord> tenders =
				tenderRecordManager.findByProductNoAndStatus(
					product.getId(),
					TenderRecord.TradeStatus.success.getCode());
			
			for(TenderRecord tender: tenders)
			{
				loanTaskService.returnTender(tender);
			}
	
			loanTaskService.processCancelProduct(product);
		}

		log.info("end checkCancelProduct");
	}
}
