package com.baibao.loan.task.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.loan.fuyou.data.ChangeCardResponse;
import com.baibao.loan.model.LoanProduct;
import com.baibao.loan.model.PublicOrder;
import com.baibao.loan.model.Repayment;
import com.baibao.loan.model.TenderRecord;
import com.baibao.loan.service.BankCardManager;
import com.baibao.loan.service.LoanProductManager;
import com.baibao.loan.service.ProfitManager;
import com.baibao.loan.service.PublicOrderManager;
import com.baibao.loan.service.RepaymentManager;
import com.baibao.loan.service.TenderRecordManager;
import com.baibao.loan.task.service.LoanTaskService;
import com.baibao.message.data.MessageType;
import com.baibao.message.service.SmsRecordManager;
import com.baibao.option.model.OptionName;
import com.baibao.option.service.OptionManager;
import com.baibao.pay.GetUniqueNoService;
import com.baibao.pay.PayService;
import com.baibao.pay.data.HostingRefundRequest;
import com.baibao.pay.data.HostingRefundResponse;
import com.baibao.user.model.User;
import com.baibao.user.model.UserInfo;
import com.baibao.user.service.UserInfoManager;
import com.baibao.user.service.UserManager;
import com.baibao.utils.HttpHelper;
import com.baibao.utils.JaxbHelper;
import com.baibao.utils.MapBuilder;
import com.baibao.utils.Utils;

import jodd.util.StringUtil;

@Service("loanTaskService")
public class LoanTaskServiceImpl
	implements LoanTaskService
{
	private static Log log = LogFactory.getLog(LoanTaskServiceImpl.class);

	@Autowired
	protected RepaymentManager repaymentManager;

	@Autowired
	protected ProfitManager profitManager;

	@Autowired
	protected OptionManager optionManager;

	@Autowired
	protected LoanProductManager loanProductManager;
	
	@Autowired
	protected TenderRecordManager tenderRecordManager;

	@Autowired
	protected PayService payService;

	@Autowired
	protected PublicOrderManager publicOrderManager;

	@Autowired
	protected GetUniqueNoService getUniqueNoService;

	@Autowired
	protected BankCardManager bankCardManager;

	@Autowired
	protected UserManager userManager;

	@Autowired
	protected UserInfoManager userInfoManager;

	@Autowired
	protected SmsRecordManager smsRecordManager;

	@Value("${service-tel}")
	protected String serviceTel;

	@Override
	@Transactional
	public Repayment processOverdue(Repayment repayment)
	{
		long overdueDays =
			Utils.betweenDays(repayment.getRepaymentTime(), Utils.now());

		if (overdueDays <= 0)
			return null;

		double overdueRate = optionManager.getDoubleValue(
			OptionName.OVERDUERATE, 0.005);
		double overdueRate2 = optionManager.getDoubleValue(
			OptionName.OVERDUERATE2, 0.01);
		long overdueDaysLimit = optionManager.getLongValue(
			OptionName.OVERDUEDAYS, 30l);

		BigDecimal total =
			repayment.getInterest().add(repayment.getCapital());
		
		repayment.setOverDueDays(overdueDays);

		long overdueDays1 = 0, overdueDays2 = 0;
		
		if (overdueDays > overdueDaysLimit)
		{
			overdueDays1 = overdueDaysLimit;
			overdueDays2 = overdueDays - overdueDaysLimit;
		} else
		{
			overdueDays1 = overdueDays;
			overdueDays2 = 0l;
		}

		BigDecimal overDueAmount1 = total
			.multiply(new BigDecimal(overdueRate))
			.multiply(new BigDecimal(overdueDays1));

		BigDecimal overDueAmount2 = total
			.multiply(new BigDecimal(overdueRate2))
			.multiply(new BigDecimal(overdueDays2));

		BigDecimal overDueAmount =
			overDueAmount1.add(overDueAmount2)
				.setScale(2, RoundingMode.HALF_UP);

		repayment.setOverDueAmount(overDueAmount);

		repayment.setPrincipal(
			overDueAmount.add(total).add(
				repayment.getOverDuePunishScore() != null?
					repayment.getOverDuePunishScore():
					new BigDecimal(0)));

		repayment.setStatus(Repayment.Status.overDue.getCode());

		repaymentManager.save(repayment);

		return repayment;
	}

	@Override
	@Transactional
	public LoanProduct processCancelProduct(LoanProduct loanProduct)
	{
		loanProduct.setStatus(LoanProduct.Status.cancel.getCode());

		loanProductManager.save(loanProduct);
		
		return loanProduct;
	}

	@Override
	@Transactional
	public TenderRecord returnTender(TenderRecord tender)
	{
		if (TenderRecord.TradeStatus.success !=
				TenderRecord.TradeStatus.valueOf(tender.getStatus()))
			return tender;

		//todo: 记录

		{//退款
			HostingRefundRequest request = new HostingRefundRequest();

			request.setReason(HostingRefundRequest.Reason.cancelMark);
			request.setAccountId(tender.getAccountNo());
			request.setAmount(
				(long) (tender.getAmount().doubleValue() * 100));
			request.setOrgiTradeNo(tender.getResponseNo());
			
			HostingRefundResponse response =
				payService.hostingRefund(request);
			if (!response.isOk())
				return tender;
		}

		tender.setStatus(TenderRecord.TradeStatus.cancel.getCode());
		tender.setTransTime(Utils.now());

		tenderRecordManager.save(tender);

		return tender;
	}

	@Override
	@Transactional
	public PublicOrder processPublicOrder(PublicOrder order)
	{
		if (StringUtil.isBlank(order.getBussType()))
			return order;

		Integer bussType = Integer.parseInt(order.getBussType());
		
		log.info(String.format("public order: %d %s",
			bussType, order.getBussDesc()));

		if (PublicOrder.BussType.recharge.getCode() == bussType)
		{
			order.setStatus(PublicOrder.Status.failed.getCode());
			
			publicOrderManager.save(order);
		} else
		if (PublicOrder.BussType.withdraw.getCode() == bussType)
		{
			order.setStatus(PublicOrder.Status.failed.getCode());
			
			publicOrderManager.save(order);
		} else
		if (PublicOrder.BussType.other.getCode() == bussType)
		{
			if ("redirect.modifyCard".equals(order.getBussDesc()))
			{
				Map<String, String> params =
					new MapBuilder<String, String>()
						.append("mchnt_cd", payService.getMerchantId())
						.append("mchnt_txn_ssn", getUniqueNoService.getCommTransFlow())
						.append("login_id", order.getCustNo())
						.append("txn_ssn", order.getFlowNo())
						.toMap();

				params.put("signature", payService.signature(new String[] {
					params.get("login_id"),
					params.get("mchnt_cd"),
					params.get("mchnt_txn_ssn"),
					params.get("txn_ssn")
				}));
				
				String url = payService.getUrl("/jzh/queryChangeCard.action");

				try
				{
					String result = HttpHelper.sendGet(url, params);

					ChangeCardResponse response =
						JaxbHelper.convert(result, ChangeCardResponse.class);

					ChangeCardResponse.Plain plain = response.getPlain();
					
					if (plain.getExamine_st().equals("1"))
					{
						order.setStatus(PublicOrder.Status.success.getCode());

						bankCardManager.updateBankCard(
							plain.getLogin_id(),
							plain.getBank_nm(), plain.getCard_no());
					} else
					if (response.getPlain().getExamine_st().equals("2"))
					{
						order.setStatus(PublicOrder.Status.failed.getCode());
					}

					publicOrderManager.save(order);
				} catch(Exception e)
				{
					log.error("error", e);
				}
				
			} else
			if ("redirect.modifyPassword".equals(order.getBussDesc()))
			{
				order.setStatus(PublicOrder.Status.success.getCode());
				
				publicOrderManager.save(order);
			} else
			if ("redirect.modifyPhone".equals(order.getBussDesc()))
			{
				order.setStatus(PublicOrder.Status.success.getCode());

				publicOrderManager.save(order);
			}
		}

		return null;
	}

	@Override
	public Repayment sendSmsRepayment(Repayment repayment)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		User user = userManager.findByCustNo(repayment.getCustNo());
		UserInfo userInfo =
			userInfoManager.findByCustNo(repayment.getCustNo());

		if (repayment.getStatus() ==
				Repayment.Status.overDue.getCode())
		{
			smsRecordManager.sendSms(
				MessageType.REPAY3, user.getMobile(),
				new MapBuilder<String, Object>()
					.append("#name#", userInfo.getRealName())
					.append("#total#", repayment.getPrincipal())
					.append("#repaymentAmount#",
						repayment.getCapital().add(repayment.getInterest()))
					.append("#overdueDays#", repayment.getOverDueDays())
					.append("#overdueFee#", repayment.getOverDueAmount())
					.append("#telephone#", serviceTel)
					.toMap());

			//【佰宝金服】亲爱的用户#name#，您在佰宝金服本期应还款#repaymentAmount#元，
			//已逾期#overdueDays#天，逾期罚息共#overdueFee#元，
			//请您积极筹措资金，尽快归还。如有疑问请你致电#telephone#
		} else
		{
			int days =
				Utils.betweenDays(
					repayment.getRepaymentTime(), Utils.now());
			
			if (days == 7 || days == 3)
			{
				smsRecordManager.sendSms(
					MessageType.REPAY1, user.getMobile(),
					new MapBuilder<String, Object>()
						.append("#name#", userInfo.getRealName())
						.append("#repaymentAmount#",
							repayment.getCapital().add(repayment.getInterest()))
						.append("#repaymentDate#",
							format.format(repayment.getRepaymentTime()))
						.append("#day#", days)
						.append("#telephone#", serviceTel)
						.toMap());
				
				//【佰宝金服】亲爱的用户#name#，您在佰宝金服本期应还款#repaymentAmount#元，
				//距离还款日#repaymentDate#还有#day#天，请您及时还款，
				//以免发生逾期，如有疑问请你致电#telephone#
			} else
			if (days == 0)
			{
				smsRecordManager.sendSms(
					MessageType.REPAY2, user.getMobile(),
					new MapBuilder<String, Object>()
						.append("#name#", userInfo.getRealName())
						.append("#repaymentAmount#",
							repayment.getCapital().add(repayment.getInterest()))
						.append("#telephone#", serviceTel)
						.toMap());

				//【佰宝金服】亲爱的用户#name#，您在佰宝金服本期应还款#repaymentAmount#元，
				//请您本日24点之前还款完成，以免发生逾期，
				//如有疑问请你致电#telephone#
			}
		}

		return repayment;
	}
}
