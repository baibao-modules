package com.baibao.loan.task.service;

import com.baibao.loan.model.LoanProduct;
import com.baibao.loan.model.PublicOrder;
import com.baibao.loan.model.Repayment;
import com.baibao.loan.model.TenderRecord;

public interface LoanTaskService
{
	public Repayment sendSmsRepayment(Repayment repayment);

	public Repayment processOverdue(Repayment repayment);

	public LoanProduct processCancelProduct(LoanProduct loanProduct);
	
	public PublicOrder processPublicOrder(PublicOrder order);

	public TenderRecord returnTender(TenderRecord tender);
}
