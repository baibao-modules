package com.baibao.user.service.impl;

import java.util.ArrayList;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.user.model.UserInfo;
import com.baibao.user.repository.UserInfoRepository;
import com.baibao.user.service.UserInfoManager;
import com.baibao.utils.Utils;
import com.baibao.utils.service.AbstractManagerImpl;

@WebService(serviceName = "userInfoManager")
@Service("userInfoManager")
public class UserInfoManagerImpl
    extends AbstractManagerImpl<UserInfo, Long>
    implements UserInfoManager
{
    @Autowired
    protected UserInfoRepository userInfoRepository;

    public void afterPropertiesSet() throws Exception {
        setRepository(userInfoRepository);
    }

	@Override
	public UserInfo findByCustNo(String custNo)
	{
		return userInfoRepository.findByCustNo(custNo);
	}
}



