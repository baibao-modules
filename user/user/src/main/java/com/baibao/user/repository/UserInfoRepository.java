package com.baibao.user.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.baibao.user.model.UserInfo;

@Repository
public interface UserInfoRepository
    extends JpaRepository<UserInfo, Long>, JpaSpecificationExecutor<UserInfo>
{
	@Query("SELECT ui FROM UserInfo ui WHERE ui.custNo = ?1")
	public UserInfo findByCustNo(String custNo);
}



