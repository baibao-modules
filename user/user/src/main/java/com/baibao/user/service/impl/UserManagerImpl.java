package com.baibao.user.service.impl;

import java.util.ArrayList;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baibao.user.model.User;
import com.baibao.user.repository.UserRepository;
import com.baibao.user.service.UserManager;
import com.baibao.utils.Utils;
import com.baibao.utils.service.AbstractManagerImpl;

@WebService(serviceName = "userManager")
@Service("userManager")
public class UserManagerImpl
    extends AbstractManagerImpl<User, Long>
    implements UserManager
{
    @Autowired
    protected UserRepository userRepository;

    public void afterPropertiesSet() throws Exception {
        setRepository(userRepository);
    }

	@Override
	public User findByCustNo(String custNo)
	{
		return userRepository.findByCustNo(custNo);
	}
}



