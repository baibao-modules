
package com.baibao.user.model;

import javax.persistence.*;

import org.apache.openjpa.persistence.jdbc.Index;

@Table(name = "p2p_users")
@Entity
public class User
{
    @Id
    @Column(name = "pu_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pu_custNo")
    private String custNo;

    @Column(name = "pu_mobile")
    private String mobile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}

