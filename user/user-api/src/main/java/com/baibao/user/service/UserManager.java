package com.baibao.user.service;

import javax.jws.WebService;
import javax.jws.WebMethod;

import com.baibao.user.model.User;
import com.baibao.utils.service.GenericManager;

@WebService
public interface UserManager
    extends GenericManager<User, Long>
{
	public User findByCustNo(String custNo);
}



