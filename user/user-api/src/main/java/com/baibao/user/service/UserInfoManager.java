package com.baibao.user.service;

import javax.jws.WebService;
import javax.jws.WebMethod;

import com.baibao.user.model.UserInfo;
import com.baibao.utils.service.GenericManager;

@WebService
public interface UserInfoManager
    extends GenericManager<UserInfo, Long>
{
	public UserInfo findByCustNo(String custNo);
}



