
package com.baibao.user.model;

import javax.persistence.*;

import org.apache.openjpa.persistence.jdbc.Index;

@Table(name = "p2p_user_info")
@Entity
public class UserInfo
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pui_custNo")
    private String custNo;
    
    @Column(name = "pui_real_name")
    private String realName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}
}

